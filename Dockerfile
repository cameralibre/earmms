FROM alpine:edge

VOLUME /config

# Install Ruby and update Bundler
RUN apk add --no-cache ruby \
  && gem install --no-document bundler:1.17.3

# Create application directory and copy in dependency lockfiles
RUN mkdir /app
COPY Gemfile Gemfile.lock package.json package-lock.json /app/
WORKDIR /app

# Install system dependencies, including development tools, run Bundler and
# npm installs, and then remove the development tools (as to not have a giant
# resulting image)
RUN apk add --no-cache \
      bash ca-certificates \
      supervisor dcron \
      ruby ruby-dev ruby-etc \
      nodejs nodejs-dev npm \
      libffi-dev libsodium-dev postgresql-dev sqlite-dev \
      libcurl curl-dev \
      alpine-sdk \
  && bundle install --deployment --with production \
  && npm install \
  && apk del alpine-sdk

# Set environment variables
ENV SSL_CERT_DIR=/etc/ssl/certs
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt

# Copy the application in
COPY . /app

# Add crontab
RUN crontab /app/docker/crontab

# Copy supervisord config to the right place
RUN mkdir -p /var/log/supervisor /etc/supervisor
COPY docker/supervisord.conf /etc/supervisor/supervisord.conf

# Build assets
RUN npm run build

# Tell Docker where our entrypoint script is
CMD /app/docker/entrypoint.sh
