# Installing earmms

## Key derivation microservice

EARMMS v2.x can use either earmms\_keyderiv v0.1.x or earmms\_keyderiv v2.x.

To use earmms\_keyderiv v0.1.x, you need to specify the `KEYDERIV_URL` 
environment variable with the URL of the service.

To use earmms\_keyderiv v2.x, you need to define multiple environment
variables:

* `KEYDERIV_URL` - the URL of the service, including the target specification
  (for example, `http://localhost:8000/?target=<TARGET>`, where `<TARGET>` 
  is the name of the target specified in the earmms\_keyderiv configuration),
* `KEYDERIV_SECRET` - the shared secret for this keyderiv target - this is
  also specified in the earmms\_keyderiv configuration file,
* `KEYDERIV_USE_2X` - this must be set to `true` to enable using v2.x of the
  key derivation microservice.

Instructions for setting up earmms\_keyderiv [can be found in it's repository][keyderiv].

## Installing dependencies and building assets

To install dependencies:

```
$ bundle install
$ npm install
```

To build assets:

```
$ npm run build
```

Note that if you don't build assets, many features of EARMMS will not work.

## Environment variables

In addition to the environment variables for the key derivation service shown
above, the following environment variables must be defined:

* `RACK_ENV`: environment to run this application in (should always be
  `production`)
* `DATABASE_URL`: The URL to the database.
* `SESSION_SECRET`: The secret key used for sessions. This should always be set
  to something, if it's left blank a new key will be used every time the app is
  restarted, which will make all sessions invalid.
* `SITE_DIR`: The site directory, containing the site theme and the configuration
  file. If defined, and a `config.rb` file exists in this directory, that file
  will be loaded at runtime.

## Setting up the database

You must run migrations before starting EARMMS, and after upgrading.

You must also run the default configuration Rake task before starting EARMMS,
and after upgrading - note that this doesn't wipe existing configuration, only
sets defaults for non-existent configuration entries.

```
$ bundle exec rake db:migrate
$ bundle exec rake cfg:defaults
```

## In-database configuration entries

See [the "Configuration" section of the documentation][configdocs] for more
information on the available configuration keys.

Most configuration values are editable either in the initial configuration
wizard, the "Configuration" page in the membership admin panel, or both -
however, you can edit the raw configuration values in the "Configuration"
page in the system admin panel.

[keyderiv]: https://gitlab.com/againstprisons/earmms_keyderiv
[configdocs]: https://againstprisons.gitlab.io/earmms/configuration/
