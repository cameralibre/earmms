import Chart from 'chart.js'

export const doCharts = () => {
  window.earmms.charts = window.earmms.charts || {}

  let chartElements = document.querySelectorAll('.js-chart')
  Array.from(chartElements).forEach((s) => {
    window.earmms.charts[s.id] = window.earmms.charts[s.id] || {}

    let data = JSON.parse(s.getAttribute('data-chart'))
    window.earmms.charts[s.id].data = data

    let chart = new Chart(s.getContext('2d'), data)
    window.earmms.charts[s.id].chart = chart
  })  
}

doCharts()
