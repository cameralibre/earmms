export const enableFieldSwitch = () => {
  let containers = document.querySelectorAll('.fieldswitch')
  Array.from(containers).forEach((ctr) => {
    let selectEl = ctr.querySelectorAll('.fieldswitch-select')[0]
    let doSwitch = () => {
      let type = selectEl.selectedOptions[0].value
      let els = ctr.querySelectorAll('.fieldswitch-field')
      Array.from(els).forEach((el) => {
        el.style.display = (el.getAttribute('data-type') == type ? 'block' : 'none')
      })
    }

    selectEl.addEventListener('change', () => doSwitch())
    doSwitch()
  })
}

enableFieldSwitch()
