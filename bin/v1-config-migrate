#!/usr/bin/env ruby
require 'json'
require 'base64'
require 'addressable'

# This constant is the current migration blob version, which is checked by, and
# must match the version of, the configuration importer in the application code.
#
# The "Version information" subheading of the "Configuration blob overview"
# section of the documentation file available at `docs/upgrading/v1-to-v2.md`
# contains a changelog of the configuration blob versions, detailing any
# extensions to the base configuration blob format.
CONFIG_MIGRATE_VERSION = 1

# This is a mocked-out EARMMS module defining the `configure` method, so that
# when the configuration file is loaded and it calls EARMMS.configure, we can
# capture the values passed in.
#
# Once the configuration file has been loaded, the configuration data is
# available at `EARMMS.cfg`.
module EARMMS
  class << self
    attr_accessor :cfg

    def configure(&block)
      cfg = {}
      block.call(cfg)

      @cfg = cfg
    end
  end
end

# This is a mocked out Mail module that lets us capture the config as passed
# to the `mail` gem. Generally, mail configuration in EARMMS v1 is done with
# something like the following:
#
#    Mail.defaults do
#      delivery_method :smtp, {
#        :address => 'smtp.gmail.com',
#        # ...
#      }
#    end
#
# This module allows us to capture the chosen delivery method (in the example
# above, `:smtp`) as well as the configuration options for that delivery method.
#
# Once the configuration file has been loaded, the delivery method is available
# at `Mail.instance.d_method` and the options for the delivery method are
# available at `Mail.instance.opts`.
module Mail
  class << self
    attr_accessor :instance

    def defaults(&block)
      @instance = Configuration.new
      @instance.instance_eval(&block)
    end
  end

  class Configuration
    attr_accessor :d_method, :opts

    def delivery_method(d_method, opts = {})
      @d_method = d_method
      @opts = opts
    end
  end
end

# Converts the settings passed in through the `Mail` mock module into a URI
# that can be set as the `email-smtp-host` key in EARMMS v2.
def convert_email_host
  # Completely ignore anything that isn't SMTP and just send that to the logger
  # instead. This matches the behaviour of the `email-smtp-host` parse step.
  if Mail.instance.d_method != :smtp
    return 'logger'
  end

  # Construct URI
  uri = Addressable::URI.new
  uri.host = Mail.instance.opts[:address]
  uri.port = Mail.instance.opts[:port]
  uri.user = Mail.instance.opts[:user_name]&.gsub("@", "%40")
  uri.password = Mail.instance.opts[:password]

  # Construct query values
  query_values = {}

  # Authentication type
  if Mail.instance.opts.key?(:authentication)
    query_values['authentication'] = Mail.instance.opts[:authentication]&.downcase
  end

  # StartTLS
  if Mail.instance.opts.key?(:enable_starttls_auto)
    query_values['starttls'] = Mail.instance.opts[:enable_starttls_auto] ? 'yes' : 'no'
  end

  # TLS
  if Mail.instance.opts.key?(:enable_tls)
    query_values['tls'] = Mail.instance.opts[:enable_tls] ? 'yes' : 'no'
  end

  # SSL
  if Mail.instance.opts.key?(:enable_ssl)
    query_values['ssl'] = Mail.instance.opts[:enable_ssl] ? 'yes' : 'no'
  end

  # OpenSSL verify mode
  if Mail.instance.opts[:openssl_verify_mode]
    mode = Mail.instance.opts[:openssl_verify_mode]
    if mode.is_a?(String)
      query_values['verify-mode'] = mode.strip.upcase
    else
      begin
        require 'openssl'
        if mode == OpenSSL::SSL::VERIFY_PEER
          query_values['verify-mode'] = 'PEER'
        elsif mode == OpenSSL::SSL::VERIFY_NONE
          query_values['verify-mode'] = 'NONE'
        else
          puts "[!!] Email: Unknown verify mode, defaulting to PEER"
        end
      rescue
        puts "[!!] Email: Non-string verify mode and couldn't require OpenSSL, defaulting to PEER"
      end
    end
  end

  # Return constructed URI
  uri.query_values = query_values
  uri.to_s
end

# Converts the custom user fields from the EARMMS mock module into a suitable
# format that can be set as the `custom-user-fields` key in EARMMS v2.
def convert_custom_user_fields
  fields = EARMMS.cfg[:custom_user_fields].map do |fd|
    desc = {
      'name' => fd[:name],
      'friendly' => fd[:friendly],
      'type' => fd[:type].to_s.strip.downcase,
      'required' => fd[:required] || false,
      'default' => fd[:default] || nil,
      'display-only-if' => {},
    }

    # Select options
    if desc['type'] == 'select'
      desc['select-options'] = fd[:select_options].map do |so|
        {
          'name' => so[:name],
          'friendly' => so[:friendly],
        }
      end
    end

    # Display only if - this covers switching based on other fields, whether to
    # show based on membership status, whether to display in user-facing profile
    # editing, and whether to display in extended signup (if enabled).

    # Display if other fields' values match the given data
    if fd.key?(:display_only_if)
      desc['display-only-if']['other-fields'] = fd[:display_only_if].map do |of|
        {
          'field' => of[:field],
          'values' => of[:values],
        }
      end
    end

    # Display if user's membership status is included in the given list
    if fd.key?(:display_only_if_membership_status)
      desc['display-only-if']['membership-status'] = fd[:display_only_if_membership_status]
    end

    # Display in profile edit?
    if fd.key?(:display_in_profile_edit)
      desc['display-only-if']['profile-edit'] = fd[:display_in_profile_edit]
    end

    # Display in extended signup?
    if fd.key?(:display_in_extended_signup)
      desc['display-only-if']['extended-signup'] = fd[:display_in_extended_signup]
    end

    desc
  end

  fields
end

def convert_discourse_sso
  sso = EARMMS.cfg[:discourse_sso]
  new = {}

  # Basic settings
  new['secret'] = sso[:secret]
  new['admin-roles'] = sso[:admin_roles].map(&:to_s)

  # Limit authentication to only callbacks that have the given hostnames
  if sso.key?(:host_whitelist)
    new['allowed-hosts'] = sso[:host_whitelist].map(&:to_s)
  end

  # Limit authentication to only users with given membership statuses
  if sso.key?(:limit_membership_status)
    new['limit-membership-status'] = sso[:limit_membership_status].map(&:to_s)
  end

  # Discourse groups, and the conditions for granting them to the authenticating
  # user.
  #
  # In v1, only one grant type could be specified (either by membership status,
  # or by role) but in v2 multiple grant types can be specified and the group is
  # only granted to the user if all of these conditions are met.
  if sso.key?(:groups)
    new['groups'] = sso[:groups].map do |gd|
    group = {
        'name' => gd[:name].to_s,
        'grant-on' => [],
      }

      if gd[:grant_on] == :membership_status
        grant = {
          'mode' => 'membership-status',
          'statuses' => gd[:statuses].map(&:to_s),
        }

        group['grant-on'] << grant
      elsif gd[:grant_on] == :role
        grant = {
          'mode' => 'role',
          'roles' => gd[:roles].map(&:to_s),
        }

        group['grant-on'] << grant
      end

      group
    end
  end

  new
end

def convert
  config = {}

  # The basics
  config['site-name'] = EARMMS.cfg[:site_name]
  config['org-name'] = EARMMS.cfg[:org_name]
  config['base-url'] = EARMMS.cfg[:base_url]

  # Email config
  config['email-from'] = EARMMS.cfg[:email_from]
  config['email-smtp-host'] = convert_email_host()

  # Extended signup and custom profile fields
  config['extended-signup-enabled'] = EARMMS.cfg[:extended_signup]
  config['field-custom-entries'] = convert_custom_user_fields()

  # Discourse SSO
  if EARMMS.cfg.key?(:discourse_sso) && EARMMS.cfg[:discourse_sso].key?(:enabled)
    config['discourse-sso-enabled'] = EARMMS.cfg[:discourse_sso][:enabled]
    config['discourse-sso-data'] = convert_discourse_sso()
  end

  # Return blob
  {
    'generator' => ['v1-config-migrate', CONFIG_MIGRATE_VERSION],
    'config' => config,
  }
end

def run(input_file, output_file)
  # load the config file
  begin
    puts "[..] Configuration file: #{input_file}"
    require input_file
  rescue => e
    puts "[!!] Exception while loading configuration file!"
    raise e
  end

  puts "[..] Loaded configuration file, starting conversion"
  output_data = convert()

  output_lines = [
    "Below is your EARMMS v2 configuration blob. When you first upgrade your",
    "EARMMS instance from v1.x to v2.x, you will be prompted to log in with",
    "an account that has system administration privileges. When you log in,",
    "you will need this configuration blob to complete setup.",
    "",
    Base64.encode64(JSON.generate(output_data)),
    "",
    "This is a version #{CONFIG_MIGRATE_VERSION} configuration blob.",
    "",
    "If you currently have a theme set, you will need to create a new config",
    "file, replacing the one you have just converted, containing the following",
    "content:",
    "",
    "EARMMS.theme_dir = '/path/to/theme'",
    "",
    "Thanks for using EARMMS!",
  ]

  puts "[..] Saving output to #{output_file}"
  File.open(output_file, "w+") do |f|
    f.write(output_lines.join("\n"))
  end

  puts "[..] Complete! Please read the output file for instructions."
end

def print_usage
  puts "Tool to convert an EARMMS v1 configuration file to a format suitable"
  puts "for importing into an EARMMS v2 installation. This tool will output a"
  puts "version #{CONFIG_MIGRATE_VERSION} configuration blob."
  puts ""
  puts "Usage: #{__FILE__} <v1 config file> <output file>"
  puts ""
  puts "Parameters:"
  puts "\t<v1 config file> - An EARMMS v1 config file (typically config.rb)"
  puts "\t<output file> - File to write converted configuration to"
end

# handle asking for help
if ARGV.length > 0 && ['-h', '-help', '--help'].include?(ARGV[0].strip)
  print_usage
  exit
end

# argument count check
if ARGV.length < 2
  puts "not enough arguments, aborting"
  print_usage
  exit 1
end

input_file = ARGV.shift
unless File.exist?(input_file)
  puts "input file does not exist, aborting"
  print_usage
  exit 1
end

output_file = ARGV.shift
if File.exist?(output_file)
  puts "output file already exists, aborting"
  print_usage
  exit 1
end

run(input_file, output_file)
