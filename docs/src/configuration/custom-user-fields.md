# Custom user fields

Custom user fields in EARMMS are defined with the `custom-user-fields`
configuration key. They can be single-line or multi-line text fields,
boolean yes/no, or dropdowns with custom options.

Custom fields are stored as JSON on the user profile object.

An example custom field description, which defines a dropdown box with
three options, is shown below:

```json
{
  "name": "test_field",
  "friendly": "Test field",
  "type": "select",
  "required": false,
  "default": "one",
  "select-options": [
    {
      "name": "one",
      "friendly": "Value one"
    },
    {
      "name": "two",
      "friendly": "Value two"
    },
    {
      "name": "three",
      "friendly": "Value three"
    }
  ],
  "display-only-if": {
    "profile-edit": true,
    "extended-signup": true
  }
}
```

The available keys are as follows:

* `name` is the internal name for the field, used when referencing the
    field in `display-only-if` configurations, and the key used when storing
    the field's value on the profile
* `friendly` is the friendly name of the field - that is, the name of the
    field that is shown to the end user
* `type` is the type of the field. The available types are:
    * `text` - a single-line text field
    * `textarea` - a multi-line text field
    * `bool` - a boolean yes/no selection
    * `select` - a dropdown box with custom values set by the `select-options` key
* `required` is a true/false value determining whether a value must be set for
    this field (this only applies to `text` and `textarea` field types)
* `default` is the default value of the field. For `select` types, this must be
    set to the internal name (the `name` key of the desired value in
    `select-options`)
* `select-options` is a list of the available options for fields with their type
    set to `select`. It is a list of hashes that contain these keys:
    * `name` is the internal name of the option, used when setting the `default`
        value of the field, and is also the raw value that is stored
    * `friendly` is the friendly name of the option, that is displayed to users in
        the dropdown selection
* `display-only-if` is a hash containing values that determine the visibility of
    the field in certain situations. See the "The `display-only-if` key" section
    below for more details.

## The `display-only-if` hash

The `display-only-if` hash is used to selectively hide fields in different
parts of the EARMMS interface.

If a key in the `display-only-if` hash is not present, it is assumed to be
`true`. All custom fields are always shown in the membership admin user edit
view.

### `profile-edit`

The `profile-edit` key determines whether or not to show the field in the
profile editing view (accessible at the path `/user`).

If it is set to `false`, the field is not shown.

### `extended-signup`

The `extended-signup` key determines whether or not to show the field in the
extended signup flow, if enabled.

If it is set to `false`, the field is not shown.

Note that in the extended signup flow, if the user selects to become a
supporter of the organisation, only fields that are marked as required are
shown to the user. If the user selects to become a member of the organisation,
all fields are shown.
