# Configuration

Configuration of EARMMS is largely done through entries in the database.

However, some core configuration, such as the path to the database, is done
with environment variables. See the [Environment variables](./environment.md)
section for details on the environment variables used.
