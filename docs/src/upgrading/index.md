# Upgrading

## Major version upgrades

Information about upgrading from EARMMS Version 1 (v1.x.x) to Version 2 (v2.x.x)
can be found at [the Version 1 to Version 2 page of this documentation][v1tov2].

[v1tov2]: ./v1-to-v2.md

## Minor version upgrades

### Version 1 (v1.x.x)

Minor/patch version upgrades are generally able to be performed without needing
to manually reconfigure the application. The most important two commands to run
to upgrade are:

```
$ bundle exec rake db:migrate
$ bundle exec rake site:default_config
```

### Version 2 (v2.x.x)

Coming soon!
