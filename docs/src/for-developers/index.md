# For developers

This section of the EARMMS documentation covers information that is
potentially useful to EARMMS developers.

If you think that something should be in this section of the documentation,
please file an issue on the GitLab repository - or, write it yourself and
send a merge request!

If you have any questions about the EARMMS codebase, please email the PAPA
Open Source team at [opensource@papa.org.nz](mailto:opensource@papa.org.nz).

## Version bumping

The EARMMS version number is in `app/version.rb`. To perform a release, edit
the version number in that file (we roughly follow SemVer), commit with a
message of "Release vX.Y.Z", and tag that commit as "vX.Y.Z". 

If you are making a merge request to EARMMS, please do not bump the version
in your MR!
