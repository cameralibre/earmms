# Development tools

There are some executables in `bin/development-scripts` that may be useful
during development. Documentation for these is below.

## `create-test-user`

The `create-test-user` script creates a new user for testing purposes. The
user created by the script has a randomly generated 7-character identifier
which is used both in the name field and as part of the user's email address.

Users created by this script have the following properties:

* Name set to `Test <IDENTIFIER> User`
* Email address set to `test.<IDENTIFIER>@example.com`
* Empty password field (meaning the user can't be used to log into the site
  without doing a password reset on the user)
* Branch set to the site's default branch
* Membership status set to supporter
* Extended signup status set to `complete`
* Custom fields set to their default values

## `mangle-v2-to-v1`

**DO NOT RUN THIS ON A PRODUCTION DATABASE! YOU HAVE BEEN WARNED.**

The `mangle-v2-to-v1` script **destructively** mangles an EARMMS v2.x
database's configuration entries and version upgrade information to
loosely resemble an EARMMS v1.x database.

This tool is meant for development of the initial setup flow - once the flow
is completed, you normally can't re-enter it. Running this tool at any point
after the v1 to v2 upgrade flow has been completed will force the application
back into upgrade lock (as it drops all version upgrade information), allowing
you to start the upgrade flow again.

The tool performs the following actions:

* Renames the following keys:
  * `membership-kaupapa-prompt` -> `kaupapa-prompt`
  * `membership-signup-member-requirements` -> `signup-member-requirements`
  * `membership-signup-supporter-requirements` -> `signup-supporter-requirements`
  * `worker-prune-old-queue-entries` -> `prune-old-work-queue-entries`
  * `worker-meeting-reminder-secs` -> `meeting-reminder-secs`
* Drops the following keys:
  * `email-smtp-host`
  * `email-from`
  * `discourse-sso-enabled`
  * `discourse-sso-data`
  * `custom-user-fields`
* Drops all version upgrade entries

The script must be run with a "magic phrase" given as it's first argument, to
protect against accidental use on a production database. This "magic phrase"
is in the comment at the top of the script - please read that comment in it's
entirety before running the script.
