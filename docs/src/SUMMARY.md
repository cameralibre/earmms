# Summary

- [Home](./index.md)
- [Configuration](./configuration/index.md)
  - [Environment variables](./configuration/environment.md)
  - [Custom user fields](./configuration/custom-user-fields.md)
  - [Dashboard quick links](./configuration/dashboard-quick-links.md)
  - [Discourse SSO](./configuration/discourse-sso.md)
- [Upgrading](./upgrading/index.md)
  - [Version 1 to Version 2](./upgrading/v1-to-v2.md)
- [For developers](./for-developers/index.md)
  - [Tools](./for-developers/tools.md)
  - [Upgrade v1 to v2](./for-developers/upgrade-v1-to-v2.md)
