Sequel.migration do
  change do
    alter_table :group_members do
      add_column :creation, DateTime, null: false, default: Sequel.function(:NOW)
    end

    alter_table :groups do
      add_column :creation, DateTime, null: false, default: Sequel.function(:NOW)
    end
  end
end
