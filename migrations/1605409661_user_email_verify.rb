Sequel.migration do
  change do
    add_column :users, :email_verified, TrueClass, null: false, default: false
  end
end
