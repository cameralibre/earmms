Sequel.migration do
  change do
    create_table :alerts do
      primary_key :id

      String :section
      String :type
      String :data
      TrueClass :actioned

      String :userid
    end
  end
end
