Sequel.migration do
  up do
    add_column :profiles, :branch_change_alert, String
  end

  down do
    drop_column :profiles, :branch_change_alert
  end
end
