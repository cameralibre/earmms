Sequel.migration do
  change do
    add_column :tokens, :expiry, DateTime
  end
end
