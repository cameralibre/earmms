Sequel.migration do
  change do
    create_table :branches do
      primary_key :id

      String :name
      String :alt_name
    end
  end
end
