Sequel.migration do
  change do
    add_column :users, :totp_secret, String
    add_column :users, :totp_enabled, TrueClass
  end
end
