Sequel.migration do
  change do
    add_column :profiles, :preferred_language, String
  end
end
