Sequel.migration do
  up do
    add_column :users, :disabled_reason, String
  end

  down do
    drop_column :users, :disabled_reason
  end
end
