Sequel.migration do
  change do
    add_column :users, :signup_extended_status_data, String, null: true
  end
end
