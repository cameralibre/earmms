Sequel.migration do
  change do
    add_column :email_queue, :attachments, String
  end
end
