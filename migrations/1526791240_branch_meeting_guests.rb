Sequel.migration do
  up do
    add_column :branch_meetings, :guests, String
  end

  down do
    drop_column :branch_meetings, :guests
  end
end
