Sequel.migration do
  up do
    add_column :users, :signup_extended_status, String
    from(:users).update(:signup_extended_status => "complete")
  end

  down do
    drop_column :users, :signup_extended_status
  end
end
