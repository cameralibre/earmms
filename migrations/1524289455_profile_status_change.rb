Sequel.migration do
  up do
    add_column :profiles, :status_change_alert, String
  end

  down do
    drop_column :profiles, :status_change_alert
  end
end
