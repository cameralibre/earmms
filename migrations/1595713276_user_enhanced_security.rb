Sequel.migration do
  change do
    alter_table :users do
      add_column :enhanced_security, TrueClass, null: false, default: false
    end
  end
end
