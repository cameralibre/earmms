Sequel.migration do
  change do
    alter_table :mass_emails do
      add_column :is_draft, TrueClass, null: false, default: false
      add_column :content_raw, String
    end
  end
end
