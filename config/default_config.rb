require 'securerandom'

module EARMMS
  APP_CONFIG_DEPRECATED_ENTRIES = {
    "worker-meeting-reminder-secs" => {
      :in => '2.0.0-rc.2',
      :reason => (
        'Moved into "worker-meeting-reminder-next", which is a time to run ' \
        'the next job, parsed by Chronic, rather than running the job some ' \
        'amount of seconds after the previous one.'
      ),
    },
  }

  APP_CONFIG_ENTRIES = {
    "site-name" => {
      :type => :text,
      :default => "EARMMS",
    },
    "org-name" => {
      :type => :text,
      :default => "Example Organisation",
    },
    "base-url" => {
      :type => :text,
      :default => "https://localhost",
    },
    "display-version" => {
      :type => :bool,
      :default => false,
    },
    "signups" => {
      :type => :bool,
      :default => false,
    },
    "signups-mailing-list-only" => {
      :type => :bool,
      :default => false,
    },
    "email-from" => {
      :type => :text,
      :default => 'earmms@example.com',
    },
    "email-smtp-host" => {
      :type => :text,
      :default => 'logger',
    },
    "email-subject-prefix" => {
      :type => :text,
      :default => 'site-name-brackets',
    },
    "mass-email-header" => {
      :type => :text,
      :default => "",
    },
    "mass-email-footer" => {
      :type => :text,
      :default => "",
    },
    "membership-kaupapa-prompt" => {
      :type => :text,
      :default => "",
    },
    "membership-information" => {
      :type => :text,
      :default => "",
    },
    "membership-default-branch" => {
      :type => :number,
      :default => '0',
    },
    "membership-welcome-email-enabled" => {
      :type => :bool,
      :default => false,
    },
    "membership-welcome-email-subject" => {
      :type => :text,
      :default => "",
    },
    "membership-welcome-email-body" => {
      :type => :text,
      :default => "",
    },
    "field-adjustments" => {
      :type => :json,
      :default => '[]',
    },
    "field-custom-entries" => {
      :type => :json,
      :default => '[]',
    },
    "extended-signup-enabled" => {
      :type => :bool,
      :default => false,
    },
    "extended-signup-member-requirements" => {
      :type => :text,
      :default => "",
    },
    "extended-signup-supporter-requirements" => {
      :type => :text,
      :default => "",
    },
    "discourse-sso-enabled" => {
      :type => :bool,
      :default => false,
    },
    "discourse-sso-data" => {
      :type => :json,
      :default => '{}',
    },
    "worker-prune-old-queue-entries" => {
      :type => :bool,
      :default => true,
    },
    "worker-meeting-reminder-next" => {
      :type => :text,
      :default => "next sunday at midnight",
    },
    "group-allow-admin-to-add-by-uid" => {
      :type => :bool,
      :default => true,
    },
    "group-user-panel-user-settings-integration" => {
      :type => :bool,
      :default => false,
    },
    "dashboard-quick-links-enabled" => {
      :type => :bool,
      :default => false,
    },
    "dashboard-quick-links-attributes" => {
      :type => :json,
      :default => '{"style": "buttons", "title": null}',
    },
    "dashboard-quick-links-content" => {
      :type => :json,
      :default => '[]',
    },
    "dashboard-custom-content-enabled" => {
      :type => :bool,
      :default => false,
    },
    "dashboard-custom-content-title" => {
      :type => :text,
      :default => '',
    },
    "dashboard-custom-content-body" => {
      :type => :text,
      :default => '',
    },
  }
end
