module EARMMS::UserTagHelpers
  require_relative './ident_helpers'
  include EARMMS::IdentHelpers

  def user_to_tag(user)
    id_tag = to_ident(user.id)
    id_tag_len = to_ident(id_tag.length)
    user_static_tag = user.get_tag

    [
      id_tag_len,
      id_tag,
      user_static_tag,
    ].join('').upcase
  end

  def tag_to_user(tag)
    id_tag_len = from_ident(tag[0..1])
    user_id = from_ident(tag[2..(id_tag_len + 1)])
    user = EARMMS::User[user_id]
    return nil if user.nil?

    user_static_tag = tag[(id_tag_len + 2)..-1].downcase
    if user.get_tag.downcase != user_static_tag
      return nil
    end

    user
  end
end