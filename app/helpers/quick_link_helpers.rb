require 'color'

module EARMMS::QuickLinkHelpers
  def quick_links_data(opts = {})
    if opts[:override]
      data = opts[:override]
    else
      data = {
        enabled: EARMMS.app_config['dashboard-quick-links-enabled'],
        attributes: EARMMS.app_config['dashboard-quick-links-attributes'],
        content: EARMMS.app_config['dashboard-quick-links-content'],
      }
    end

    # Bail if quick links aren't enabled
    return nil unless data[:enabled]

    # Allow overriding the content of the link group <h1>
    title = data[:attributes]['title']
    title = t(:'dashboard/quick_links') if title.nil? || title.empty?

    # Link style is one of the following:
    #
    # * "buttons" - this is a list of full-page-width buttons
    # * "tiles" - an array of rectangular tiles of a fixed width. The link
    #   icon is positioned at the top left of the tile, and the text is along
    #   the bottom.
    link_style = data[:attributes]['style']
    link_style = "buttons" unless %w[buttons tiles].include?(link_style)

    # Styles to inject
    inject_styles = {}

    # Construct link data
    links = data[:content].map do |ldesc|
      ldesc = ldesc.map{|k, v| [k.tr('-', '_').to_sym, v]}.to_h
      ldesc[:classes] ||= []
      ldesc[:data] ||= {}

      # Membership status check
      unless ldesc[:only_statuses].nil? || ldesc[:only_statuses].empty?
        if opts[:profile]
          status = opts[:profile].decrypt(:membership_status)&.strip&.downcase
          unless ldesc[:only_statuses].include?(status)
            next nil
          end
        end
      end

      # Button colour
      if ldesc[:colour].nil? || ldesc[:colour].empty?
        ldesc[:classes] << "ql--nocolour"
      elsif %w[primary success warning error].include?(ldesc[:colour])
        ldesc[:classes] << "button-#{ldesc[:colour]}"
      else
        colour_key = Base32.encode(ldesc[:colour]).gsub('=', '').downcase

        ldesc[:classes] << "ql--customcolour"
        ldesc[:classes] << "ql--customcolour--#{colour_key}"
        inject_styles[".ql--customcolour--#{colour_key}"] ||= []
        inject_styles[".ql--customcolour--#{colour_key}"] << "background-color: #{ldesc[:colour]}"

        # Check for lightness
        colour = Color::RGB.by_css(ldesc[:colour]).to_hsl
        if colour.lightness > 80
          ldesc[:classes] << 'ql--link-dark'
        end
      end

      # Icon visibility / classes
      if ldesc[:icon].nil? || ldesc[:icon].empty?
        ldesc[:icon] = {visible: false}
      else
        # Font Awesome icons
        if ldesc[:icon].start_with?('fa-')
          ldesc[:icon] = {visible: true, classes: ["fa", ldesc[:icon]]}
        end
      end

      # Set some defaults if the link description doesn't include them
      ldesc[:text] = t(:'unknown') if ldesc[:text].nil? || ldesc[:text].empty?
      ldesc[:href] = '#' if ldesc[:href].nil? || ldesc[:href].empty?

      ldesc
    end.compact

    # Assemble the CSS to inject
    assembled_styles = inject_styles.map do |selector, styles|
      styles = styles.map(&:strip).join("; ")
      "#{selector} { #{styles} }"
    end.join("\n").strip

    {
      title: title,
      style: link_style,
      links: links,
      inject_styles: assembled_styles,
    }
  end
end
