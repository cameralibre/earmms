require 'json'

module EARMMS::InitialSetupUpgradeHelpers
  def upgrade_steps
    [
      {
        :status => 'not-started',
        :path => '/initialsetup/upgrade',
        :friendly => t(:'initial_setup/upgrade/steps/blob'),
        :can_discard => true,
      },
      {
        :status => 'blob-okay',
        :path => '/initialsetup/upgrade/change',
        :friendly => t(:'initial_setup/upgrade/steps/verify'),
        :can_discard => true,
      },
      {
        :status => 'verified',
        :path => '/initialsetup/upgrade/parse',
        :friendly => t(:'initial_setup/upgrade/steps/parse'),
        :can_discard => true,
      },
      {
        :status => 'parsed',
        :path => '/initialsetup/upgrade/custom-field-demo',
        :friendly => t(:'initial_setup/upgrade/steps/custom_field_demo'),
        :can_discard => true,
      },
      {
        :status => 'fields-okay',
        :path => '/initialsetup/upgrade/apply',
        :friendly => t(:'initial_setup/upgrade/steps/apply'),
        :can_discard => true,
      },
      {
        :status => 'applied',
        :path => '/initialsetup/upgrade/complete',
        :friendly => t(:'initial_setup/upgrade/steps/complete'),
        :can_discard => false,
      },
    ]
  end

  def upgrade_construct_changes(blob)
    # Construct a list of the configuration changes we're going to make.
    # `changes` is a list of hashes of the following format:
    #
    # {
    #   :source => (:upgrade | :blob | :default),
    #   :type => (:add | :remove | :modify | :rename)
    #   :old_key => ("old-key" | nil),
    #   :new_key => ("new-key" | nil)
    #   :old_value => ("old-value" | nil),
    #   :new_value => ("new-value | nil),
    # }
    #
    # If a key is being added:\
    #   * `:type` is-`:add`
    #   * `:new_key` contains the new key
    #   * `:new_value` contains the new value
    # If a key is being removed:
    #   * `:type` is `:remove`
    #   * `:old_key` contains the original key
    #   * `:old_value` contains the original value
    # If a key is changing value without renaming:
    #  * `:type` is `:modify`
    #  * `:new_key` contains the key
    #  * `:old_value` contains the original value
    #  * `:new_value` contains the new value
    # If a key is being renamed without changing value:
    #  * `:type` is `:rename`
    #  * `:old_key` contains the original key
    #  * `:new_key` contains the new key
    #  * `:new_value` contains the value
    # If a key is being renamed AND is changing value:
    #  * `:type` is `:rename`
    #  * `:old_key` contains the original key
    #  * `:new_key` contains the new key
    #  * `:old_value` contains the original value
    #  * `:new_value` contains the new value
    changes = []

    # Add the keys being removed to the changes list
    removal_keys = [
      '2fa-required-for-roles',
      'email-to-self',
      'signup-twocolumn',
      
      # XXX: This was renamed (and the way it works changed) late in v2 dev.
      # It's easier to delete it and repopulate it and let the default value
      # repopulate.
      'worker-meeting-reminder-secs',
    ]
    removal_keys.each do |key|
      val = get_config_value(key)
      next if val.nil?
      changes << {
        :source => :upgrade,
        :type => :remove,
        :old_key => key,
        :old_value => val,
      }
    end

    # Add the renamed keys to the changes list
    rename_keys = {
      'prune-old-work-queue-entries' => 'worker-prune-old-queue-entries',
      'meeting-reminder-secs' => 'worker-meeting-reminder-secs',
      'kaupapa-prompt' => 'membership-kaupapa-prompt',
      'signup-member-requirements' => 'extended-signup-member-requirements',
      'signup-supporter-requirements' => 'extended-signup-supporter-requirements',

      # XXX: This was renamed half way into v2 dev, putting it here just in case
      # someone tries to do an upgrade with a v1 blob that was generated early
      # in the dev process
      'custom-user-fields' => 'field-custom-entries',
    }
    rename_keys.each do |old, new|
      val = get_config_value(old)
      next if val.nil?
      changes << {
        :source => :upgrade,
        :type => :rename,
        :old_key => old,
        :new_key => new,
        :new_value => val,
      }
    end

    # Add the configuration entries from the given configuration blob to the
    # changes list. We're reversing this and then unshifting onto the changes
    # list so that the config entries from the configuration blob show up at
    # the top of the changes list
    blob['config'].each.to_a.reverse.each do |key, value|
      if EARMMS::APP_CONFIG_ENTRIES[key][:type] == :json
        value = JSON.generate(value)
      elsif EARMMS::APP_CONFIG_ENTRIES[key][:type] == :number
        value = value.to_s
      elsif EARMMS::APP_CONFIG_ENTRIES[key][:type] == :bool
        value = (value ? 'yes' : 'no')
      end

      changes.unshift({
        :source => :blob,
        :type => :add,
        :new_key => key,
        :new_value => value,
      })
    end

    # Go over application defaults and add new values for any of the entries
    # in APP_CONFIG_ENTRIES that do not have a key already in the changes
    existing_keys = changes.map{|c| c[:new_key]}.compact
    EARMMS::APP_CONFIG_ENTRIES.each do |key, desc|
      next if existing_keys.include?(key)
      next unless get_config_value(key).nil?

      value = desc[:default]
      if desc[:type] == :number
        value = value.to_s
      elsif desc[:type] == :bool
        value = (value ? 'yes' : 'no')
      end

      changes << {
        :source => :default,
        :type => :add,
        :new_key => key,
        :new_value => value,
      }
    end

    changes
  end

  def upgrade_construct_mock_db(changes)
    # Gather all our existing database configuration into a hash to use as a
    # mock database for applying changes to and running the parse step on
    mockdb = EARMMS::ConfigModel.all.map do |cfg|
      [
        cfg.key,
        {
          :key => cfg.key,
          :type => cfg.type.downcase.to_sym,
          :value => cfg.value,
          :source => :existing,
        }
      ]
    end.to_h

    # Gather changes and apply changes to our mock database
    changes.each do |change|
      if change[:type] == :add || change[:type] == :modify
        mockdb[change[:new_key]] = {
          :key => change[:new_key],
          :type => EARMMS::APP_CONFIG_ENTRIES[change[:new_key]][:type],
          :value => change[:new_value],
          :source => change[:source],
        }
      elsif change[:type] == :rename
        mockdb[change[:old_key]] = nil
        mockdb[change[:new_key]] = {
          :key => change[:new_key],
          :type => EARMMS::APP_CONFIG_ENTRIES[change[:new_key]][:type],
          :value => change[:new_value],
          :source => change[:source],
        }
      elsif change[:type] == :remove
        mockdb[change[:old_key]] = nil
      end
    end

    mockdb
  end
end
