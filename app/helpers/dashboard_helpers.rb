module EARMMS::DashboardHelpers
  def branch_meetings
    EARMMS::Branch.all.map do |b|
      now = DateTime.now()
      yesterday = now - 1
      yesterday = DateTime.civil(yesterday.year, yesterday.month, yesterday.day, 23, 59, 59)
      twoweeks = yesterday + 14

      upcoming = EARMMS::BranchMeeting.where(branch: b.id).order(Sequel.desc(:id)).limit(5).map do |m|
        dt = DateTime.parse(m.decrypt(:datetime))
        next nil unless dt.between?(yesterday, twoweeks)

        {
          :id => m.id,
          :branch => b.id,
          :datetime => dt,
        }
      end.compact

      [
        b.id,
        {
          :id => b.id,
          :name => b.decrypt(:name),
          :upcoming => upcoming,
        }
      ]
    end.compact.to_h
  end

  def custom_content_data
    # Bail if custom content isn't enabled
    return nil unless EARMMS.app_config['dashboard-custom-content-enabled']
    
    {
      title: EARMMS.app_config['dashboard-custom-content-title'],
      body: EARMMS.app_config['dashboard-custom-content-body'],
    }
  end
end
