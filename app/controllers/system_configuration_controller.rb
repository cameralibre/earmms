class EARMMS::SystemConfigurationController < EARMMS::SystemController
  helpers EARMMS::SystemConfigurationHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:change_config'
    @configentries = EARMMS::ConfigModel.all
    @deprecated = get_deprecated(@configentries)
  end

  get '/' do
    @title = t(:'system/configuration/title')
    haml :'system/config/index'
  end

  post '/edit' do
    key = validate_cfgkey(request.params["key"]&.strip&.downcase)
    if key.nil?
      flash :error, t(:'system/configuration/edit/errors/invalid_key')
      next redirect '/system/config'
    end

    next redirect "/system/config/edit/#{key}"
  end

  get '/edit/:key' do |key|
    @key = validate_cfgkey(key)
    halt 400 if @key.nil?

    @deprecated = get_deprecated(@configentries + [@key])
    @entry = EARMMS::ConfigModel.where(key: @key).first
    @type = @entry&.type&.to_sym
    @value = @entry&.value
    
    @title = t(:'system/configuration/edit/title', :key => @key)
    haml :'system/config/edit'
  end

  post '/edit/:key' do |key|
    @key = validate_cfgkey(key)
    halt 400 if @key.nil?

    @deprecated = get_deprecated(@configentries + [@key])
    @entry = EARMMS::ConfigModel.where(key: @key).first
    @type = @entry&.type&.to_sym
    @value = @entry&.value

    if request.params["delete"]&.strip&.downcase == "on"
      if @entry || EARMMS.app_config.include?(@key)
        # verify key name from text box
        unless request.params["verify"]&.strip&.downcase == @key
          flash :error, t(:'system/configuration/edit/delete/errors/verify_failed', :key => @key)
          next redirect request.path
        end
        
        # delete the entry
        @entry.delete

        # generate an alert
        gen_alert(:system, :config_change, t(:'system/configuration/edit/delete/alert', {
          :key => @key,
          :force_language => true,
        }))

        # add to refresh pending
        unless EARMMS.app_config_refresh_pending.include?(@key) 
          EARMMS.app_config_refresh_pending << @key
        end

        flash :success, t(:'system/configuration/edit/delete/alert', :key => @key)
      else
        flash :success, t(:'system/configuration/edit/cancel_creation/alert', :key => @key)
      end

      next redirect '/system/config'
    end

    # Delay the `find_or_create` until after the potential cancellation
    # of creating a new key before it's been saved
    @entry ||= EARMMS::ConfigModel.find_or_create(key: @key)

    if request.params.key?("value") || request.params.key?("expanded")
      type = request.params["type"]&.strip&.downcase&.to_sym
      if !(type.nil? || @type.nil?) && @type != type
        flash :error, t(:'system/configuration/edit/change/errors/no_change_type')
        next haml :'system/config/edit'
      end
      @type = type

      if request.params["expanded"]&.strip&.downcase == "on"
        next "bad expanded key #{@type.to_s.inspect}" unless request.params.key?("value-#{@type.to_s}")
        @value = request.params["value-#{@type.to_s}"]&.strip
      else
        @value = request.params["value"]&.strip
      end

      @entry.type = @type
      @entry.value = @value
      @entry.save

      unless EARMMS.app_config_refresh_pending.include?(@key)
        EARMMS.app_config_refresh_pending << @key
      end

      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @key,
        :value => @value,
        :force_language => true,
      }))

      flash :success, t(:'system/configuration/edit/change/success', :key => @key)
    end

    next redirect request.path
  end
end
