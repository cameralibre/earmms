class EARMMS::UserGroupsLeaveController < EARMMS::ApplicationController
  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      next redirect('/auth')
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    # Get groups
    @my_groups = EARMMS::Group.hashes_for_user(@user)
  end
  
  post '/' do
    @group = EARMMS::Group[request.params['group'].to_i]
    next halt 404 unless @group
    @group_h = @my_groups.select{|x| x[:group_id] == @group.id}.first
    next halt 404 unless @group_h

    if @group_h[:user_group_roles].include?('admin')
      flash :error, t(:'usergroups/leave/errors/is_admin')
      next redirect "/ugroups/view/#{@group_h[:group_id]}"
    end

    if request.params['confirm']&.strip&.downcase == 'on'
      gm = @group_h[:user_group_member]
      EARMMS::GroupMemberFilter.clear_filters_for(gm)
      gm.delete

      flash :success, t(:'usergroups/leave/success')
      next redirect '/ugroups'
    end

    @title = t(:'usergroups/leave/title')
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'user_groups/leave', :layout => false
    end
  end
end
