class EARMMS::UserSettingsMfaRecoveryController < EARMMS::ApplicationController
  helpers EARMMS::AuthMfaRecoveryHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect '/auth'
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @has_totp = @user.totp_enabled
    next redirect('/user/mfa') unless @has_totp

    @has_u2f = EARMMS::U2FRegistration.where(user: @user.id).count.positive?
    @has_roles = EARMMS::UserHasRole.where(user: @user.id).count.positive?
    @has_recovery = user_has_mfa_recovery?(@user)

    if @enhanced = @user.enhanced_security
      flash :error, t(:'userprofile/mfa/recovery/enhanced')
      next redirect '/user/mfa'
    end

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
  end

  post '/' do
    @title = t(:'userprofile/mfa/recovery/title')

    @codes = []
    if !@has_recovery || request.params['regen'].to_i == 1
      @codes = generate_mfa_recovery!(@user)
    end

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'user_settings/mfa/recovery', :layout => false
    end
  end
end
