class EARMMS::MembershipAdminConfigurationDashboardContentController < EARMMS::MembershipAdminController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'
    
    @enabled_entry = EARMMS::ConfigModel.where(key: 'dashboard-custom-content-enabled').first
    @header_entry = EARMMS::ConfigModel.where(key: 'dashboard-custom-content-title').first
    @body_entry = EARMMS::ConfigModel.where(key: 'dashboard-custom-content-body').first

    if @enabled_entry.nil? || @header_entry.nil? || @body_entry.nil?
      flash :error, t(:'membership/configuration/dashboard_content/errors/non_existent_entries')
      next redirect '/admin/config'
    end
    
    @is_enabled = @enabled_entry.value == 'yes'
    @title = t(:'membership/configuration/dashboard_content/title')
  end

  get '/' do
    haml :'membership_admin/config/dashboard_content'
  end

  post '/' do
    enabled = request.params['enabled']&.strip&.downcase == 'yes'
    if enabled
      if @enabled_entry.value == 'no'
        @enabled_entry.value = 'yes'
        @enabled_entry.save
        
        gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
          :key => @enabled_entry.key,
          :value => @enabled_entry.value,
          :force_language => true,
        }))
      end
    else
      if @enabled_entry.value == 'yes'
        @enabled_entry.value = 'no'
        @enabled_entry.save
        
        gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
          :key => @enabled_entry.key,
          :value => @enabled_entry.value,
          :force_language => true,
        }))
      end
        
      flash :success, t(:'membership/configuration/dashboard_content/success/disabled')
      next redirect request.path
    end

    # change header
    header_changed = false
    header = request.params["header"]&.strip
    header_fragment = Sanitize.fragment(header).gsub("&nbsp;", " ").strip
    if header.nil? || header&.empty? || header_fragment.empty?
      header_changed = true if @header_entry.value != ''
      @header_entry.value = ''
    else
      header_changed = true if @header_entry.value != header
      @header_entry.value = header
    end

    if header_changed
      @header_entry.save
      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @header_entry.key,
        :value => @header_entry.value,
        :force_language => true,
      }))
    end

    # change body
    body_changed = false
    body = request.params["body"]&.strip
    body_fragment = Sanitize.fragment(body, Sanitize::Config::RESTRICTED).gsub("&nbsp;", " ").strip
    if body.nil? || body&.empty? || body_fragment.empty?
      body_changed = true if @body_entry.value != ''
      @body_entry.value = ''
    else
      body_changed = true if @body_entry.value != body
      @body_entry.value = body
    end

    if body_changed
      @body_entry.save
      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @body_entry.key,
        :value => @body_entry.value,
        :force_language => true,
      }))
    end

    flash :success, t(:'membership/configuration/dashboard_content/success')
    next redirect request.path
  end
end
