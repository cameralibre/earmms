require 'base64'
require 'json'
require 'addressable'

class EARMMS::InitialSetupUpgradeCustomFieldsController < EARMMS::InitialSetupController
  get '/' do
    begin
      @blob = JSON.parse(@this_upgrade.decrypt(:data))
    rescue
      # This should never happen!
      next halt 500
    end

    # If there are no custom fields defined, skip over this step
    if @blob['config']['field-custom-entries'].nil? ||  @blob['config']['field-custom-entries'].empty?
      flash :warning, t(:'initial_setup/upgrade/custom_field_demo/skipped')

      @this_upgrade.status = 'fields-okay'
      @this_upgrade.save
      next redirect current_step_url
    end

    # Generate custom field descriptions from the blob
    @fields = custom_field_desc(:data => @blob['config']['field-custom-entries'])

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/upgrade/layout', :layout => false do
        haml :'initial_setup/upgrade/custom_field_demo', :layout => false
      end
    end
  end

  post '/' do
    # Custom field demo was approved, save status as "fields-okay" and move to
    # the final accept step!
    @this_upgrade.status = 'fields-okay'
    @this_upgrade.save

    next redirect current_step_url
  end
end
