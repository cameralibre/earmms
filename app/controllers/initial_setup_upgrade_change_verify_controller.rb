require 'base64'
require 'json'
require 'addressable'

class EARMMS::InitialSetupUpgradeChangeVerifyController < EARMMS::InitialSetupController
  get '/' do
    begin
      @blob = JSON.parse(@this_upgrade.decrypt(:data))
    rescue
      # This should never happen!
      next halt 500
    end

    # Gather changes
    @changes = upgrade_construct_changes(@blob)

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/upgrade/layout', :layout => false do
        haml :'initial_setup/upgrade/change_verify', :layout => false
      end
    end
  end

  post '/' do
    # Accepted changes, save status as 'verified' and move on to parse step
    @this_upgrade.status = 'verified'
    @this_upgrade.save
    next redirect current_step_url
  end
end
