require 'rotp'

class EARMMS::AuthLoginMfaTotpController < EARMMS::ApplicationController
  before do
    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]

    next halt redirect('/') if logged_in?

    next halt 400 unless session[:uid]
    @u = EARMMS::User[session[:uid].to_i]
    next halt 400 unless @u
    next halt 400 unless @u.totp_enabled

    @title = t(:'auth/login/mfa/title')
    @has_u2f = EARMMS::U2FRegistration.where(user: @u.id).count.positive?

    # Enhanced security mode requires the user to use security-key based MFA
    if @enhanced = @u.enhanced_security
      unless @has_u2f
        flash :error, t(:'auth/login/enhanced/errors/no_keys')
        out = haml(:'auth/layout', layout: :layout_minimal) {""}
        next halt 400, out
      end

      next redirect '/auth/mfa/key'
    end

    @debug_bypass = settings.development? && (@u.decrypt(:totp_secret) == '#DEBUG BYPASS#')
  end

  get '/' do
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/login/totp', :layout => false
    end
  end

  post '/' do
    code = request.params["code"]&.strip&.downcase
    if code.nil? || code.empty?
      flash :error, t(:'auth/login/mfa/totp/errors/no_code')
      next redirect request.path
    end
    code = code.split(' ').map{|x| x.split('-')}.flatten.join('').strip.downcase

    success = false
    if code.length == EARMMS::Token::SHORT_TOKEN_LENGTH
      # Recovery code?
      rc = EARMMS::Token.where(user: @u.id, token: code).first
      if rc&.is_valid?
        rc.invalidate!
        rc.save
        success = true

        session[:after_login] = '/user/mfa'
        flash :success, t(:'auth/login/success/recovery')
      end
    else
      totp_secret = @u.decrypt(:totp_secret)
      if totp_secret == '#DEBUG BYPASS#'
        if settings.development?
          success = true
          flash :success, t(:'auth/login/success/debug_bypass')
        end
      else
        t = ROTP::TOTP.new(totp_secret)
        if t.verify(code, :drift_behind => 15)
          success = true
          flash :success, t(:'auth/login/success')
        end
      end
    end

    if success
      token = EARMMS::Token.generate_session(@u)
      session.delete(:uid)
      session[:token] = token.token
      token.save

      redir_to = session.delete(:after_login)
      redir_to = '/' unless redir_to
      next redirect(redir_to)

    else
      flash :error, t(:'auth/login/mfa/totp/errors/invalid_code')
      next redirect request.path
    end
  end
end
