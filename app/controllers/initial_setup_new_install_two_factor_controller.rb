require 'rotp'
require 'rqrcode'
require 'base64'

class EARMMS::InitialSetupNewInstallTwoFactorController < EARMMS::InitialSetupController
  get '/' do
    @user = current_user
    @data = JSON.parse(@this_upgrade.decrypt(:data))

    if current_user.totp_enabled
      @this_upgrade.status = 'two-factor-ok'
      @this_upgrade.save

      flash :warning, t(:'initial_setup/newinstall/two_factor/skipped')
      next redirect '/initialsetup/newinstall/roles'
    end

    if !session.key?(:totp_secret)
      session[:totp_secret] = ROTP::Base32.random_base32
    end

    t = ROTP::TOTP.new(session[:totp_secret], :issuer => @data['config-entries']['site-name'])
    url = t.provisioning_uri(@user.email)
    qr = RQRCode::QRCode.new(url)
    svg = qr.as_svg(offset: 0, color: '000', shape_rendering: 'crispEdges', module_size: 11)
    svg_b64 = Base64.encode64(svg)
    @qrcode_data = "data:image/svg+xml;base64,#{svg_b64}"

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/two_factor', :layout => false
      end
    end
  end

  post '/' do
    @user = current_user
    @data = JSON.parse(@this_upgrade.decrypt(:data))

    if current_user.totp_enabled
      @this_upgrade.status = 'two-factor-ok'
      @this_upgrade.save

      flash :warning, t(:'initial_setup/newinstall/two_factor/skipped')
      next redirect '/initialsetup/newinstall/roles'
    end

    unless session.key?(:totp_secret)
      next redirect request.path
    end

    code = request.params['code']&.strip&.downcase
    if code.nil? || code&.empty?
      flash :error, t(:'initial_setup/newinstall/two_factor/errors/invalid_code')
      next redirect request.path
    end

    if settings.development? && code == 'devmode_skip'
      flash :warning, t(:'initial_setup/newinstall/two_factor/devmode_skip/ok')

      @this_upgrade.status = 'two-factor-ok'
      @this_upgrade.save
      next redirect '/initialsetup/newinstall/roles'
    end

    t = ROTP::TOTP.new(session[:totp_secret])
    if t.verify(code, :drift_behind => 15)
      @user.encrypt(:totp_secret, session.delete(:totp_secret))
      @user.totp_enabled = true
      @user.save
    else
      flash :error, t(:'initial_setup/newinstall/two_factor/errors/invalid_code')
      next redirect request.path
    end

    @this_upgrade.status = 'two-factor-ok'
    @this_upgrade.save
    next redirect '/initialsetup/newinstall/roles'
  end
end
