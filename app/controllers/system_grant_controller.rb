class EARMMS::SystemGrantController < EARMMS::SystemController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:grant'
  end

  get '/' do
    @title = t(:'system/grant/title')
    haml :'system/grant/index'
  end

  get '/list' do
    @users = {}
    case request.params["type"]&.strip&.downcase
    when "all"
      @criteria = [:any, nil]
      EARMMS::UserHasRole.each do |uhr|
        unless @users.key?(uhr.user)
          @users[uhr.user] = {
            user: EARMMS::User[uhr.user],
            roles: []
          }

          @users[uhr.user][:name] = EARMMS::Profile.for_user(@users[uhr.user][:user]).decrypt(:full_name)
        end

        @users[uhr.user][:roles] << uhr
      end

    when "role"
      next halt 400, "no role key" unless request.params.key?("role")
      role = request.params["role"].strip.downcase
      @criteria = [:role, role]

      EARMMS::UserHasRole.where(role: role).each do |uhr|
        unless @users.key?(uhr.user)
          @users[uhr.user] = {
            user: EARMMS::User[uhr.user],
            roles: []
          }

          @users[uhr.user][:name] = EARMMS::Profile.for_user(@users[uhr.user][:user]).decrypt(:full_name)
        end

        EARMMS::UserHasRole.where(user: uhr.user).each do |r|
          @users[uhr.user][:roles] << r
        end
      end

    else
      next halt 400, "bad type"
    end
    
    @title = t(:'system/grant/search/results/title')
    haml :'system/grant/list'
  end

  get '/edit' do
    @user = EARMMS::User[request.params["uid"].to_i]
    unless @user
      flash :error, t(:'system/grant/edit/errors/no_user_with_that_id')
      next redirect '/system/grant'
    end

    @user_name = EARMMS::Profile.for_user(@user).decrypt(:full_name)
    @has_2fa = @user.totp_enabled || (EARMMS::U2FRegistration.where(user: @user.id).count > 0)
    @title = t(:'system/grant/edit/title', :user => @user_name)

    haml :'system/grant/edit'
  end

  post '/edit' do
    @user = EARMMS::User[request.params["uid"].to_i]
    unless @user
      flash :error, t(:'system/grant/edit/errors/no_user_with_that_id')
      next redirect '/system/grant'
    end
   
    @user_name = EARMMS::Profile.for_user(@user).decrypt(:full_name)
    @has_2fa = @user.totp_enabled || (EARMMS::U2FRegistration.where(user: @user.id).count > 0)
    @title = t(:'system/grant/edit/title', :user => @user_name)

    r = request.params["role"]&.strip&.downcase
    if r.nil? || r.empty?
      flash :error, t(:'system/grant/errors/no_role')
      next haml :'system/grant/edit'
    end

    case request.params["action"]&.strip&.downcase
    when "delete"
      EARMMS::UserHasRole.where(user: @user.id, role: r).delete

      gen_alert :system, :role_edit, t(:'system/grant/edit/remove/success', {
        :role => r,
        :user => @user_name,
        :uid => @user.id,
        :force_language => true
      })

      flash :success, t(:'system/grant/edit/remove/success', {
        :role => r,
        :user => @user_name,
        :uid => @user.id,
      })

    when "add"
      unless @has_2fa
        flash :error, t(:'system/grant/edit/errors/no_mfa')
        next haml :'system/grant/edit'
      end

      uhr = EARMMS::UserHasRole.new(user: @user.id, role: r)
      uhr.save

      gen_alert :system, :role_edit, t(:'system/grant/edit/add/success', {
        :role => r,
        :user => @user_name,
        :uid => @user.id,
        :force_language => true
      })

      flash :success, t(:'system/grant/edit/add/success', {
        :role => r,
        :user => @user_name,
        :uid => @user.id,
      })

    else
      next halt 400, "invalid action"
    end

    next haml :'system/grant/edit'
  end
end
