class EARMMS::MembershipAdminGroupDeleteController < EARMMS::MembershipAdminController
  helpers EARMMS::GroupHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group:delete'

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
  end

  get '/:id' do |id|
    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group
    @group_h = @group.to_h

    # Generate a verification code and store it in the session if one
    # doesn't already exist there
    if session.key?(:group_delete_verify_code)
      @verify_code = session[:group_delete_verify_code]
    else
      @verify_code = Random.new.rand(100000000 .. 999999999).to_s
      session[:group_delete_verify_code] = @verify_code
    end

    @title = t(:'membership/groups/delete/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'membership_admin/group/delete', :layout => false
    end
  end
  
  post '/:id' do |id|
    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group
    @group_h = @group.to_h
    
    # Check verification code
    form_verify = request.params['verify']&.strip
    if form_verify
      form_verify = form_verify.split(' ').map{|x| x.split('-')}.flatten.join('')
    end
    if form_verify != session[:group_delete_verify_code]
      flash :error, t(:'membership/groups/delete/errors/invalid_code')
      next redirect request.path
    end
    session.delete(:group_delete_verify_code)
    
    @group.delete!
    flash :success, t(:'membership/groups/delete/success')
    redirect "/admin/group"
  end
end
