class EARMMS::MembershipAdminUserChangelogController < EARMMS::MembershipAdminController
  helpers EARMMS::UserChangelogHelpers

  get '/:id' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name)
    @title = t(:'membership/user/changelog/title', :user => @user_name)

    @display_raw = request.params['raw'].to_i.positive?
    @changelog_entries = changelog_entries_for_profile(@profile)

    haml :'membership_admin/user/changelog'
  end
end
