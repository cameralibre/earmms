require 'email_validator'

class EARMMS::AuthSignupMailingListController < EARMMS::ApplicationController
  helpers EARMMS::AuthSignupHelpers
  helpers EARMMS::BranchHelpers

  before do
    next redirect '/auth/signup' unless EARMMS.app_config['signups']
    next redirect '/auth/signup' unless EARMMS.app_config['signups-mailing-list-only']
    next redirect '/' if logged_in?

    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @branches = get_branches_data(ignore_roles: true)
    @default_branch = @branches.find { |b| b[:is_default] }
  end

  get '/' do
    @title = t(:'auth/signup/mailing_list/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/mailing_list/index', :layout => false
    end
  end

  post '/' do
    email = request.params["email"]&.strip&.downcase
    if email.nil? || email&.empty? || !(EmailValidator.valid?(email))
      flash :error, t(:'auth/signup/mailing_list/errors/invalid_email')
      next redirect request.path
    end

    if EARMMS::User.where(email: email).count.positive?
      flash :error, t(:'auth/signup/mailing_list/errors/already_exists')
      next redirect request.path
    end

    uname = request.params['name']&.strip
    if uname.nil? || uname&.empty?
      flash :error, t(:'auth/signup/mailing_list/errors/no_name')
      next redirect request.path
    end

    branch = EARMMS::Branch[request.params['branch']&.strip.to_i]
    unless branch
      flash :error, t(:'auth/signup/mailing_list/errors/invalid_branch')
      next redirect request.path
    end

    # Create user
    user = EARMMS::User.new
    user.email = email
    user.password_hash = nil
    user.creation = Time.now
    user.signup_extended_status = "complete"
    user.save

    # Create profile
    profile = EARMMS::Profile.new(user: user.id)
    profile.save
    profile.encrypt(:full_name, uname)
    profile.encrypt(:branch, branch.id)
    profile.encrypt(:membership_status, "supporter")
    profile.encrypt(:custom_fields, custom_field_default_json)
    profile.save

    # Create profile filters
    EARMMS::ProfileFilter.create_filters_for(profile)

    # Send verification email
    user.send_email_verification!

    # Successful!
    @title = t(:'auth/signup/mailing_list/success/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/mailing_list/success', :layout => false
    end
  end
end
