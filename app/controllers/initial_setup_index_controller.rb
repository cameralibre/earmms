class EARMMS::InitialSetupIndexController < EARMMS::InitialSetupController
  get '/' do
    unless @this_upgrade.status.nil? || @this_upgrade.status == 'not-started'
      next redirect current_step_url()
    end

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/index', :layout => false
    end
  end

  post '/force-acquire' do
    @mutex[:mutex].release!
    @mutex = EARMMS::MutexModel.acquire!(current_user.id, 'initial_setup')

    flash :success, t(:'initial_setup/lock/acquired')
    redirect current_step_url()
  end

  post '/discard' do
    unless current_step_can_discard
      flash :error, t(:'initial_setup/discard_and_return/failed')
      next redirect current_step_url || '/initialsetup'
    end

    @this_upgrade.mode = nil
    @this_upgrade.status = 'not-started'
    @this_upgrade.data = nil
    @this_upgrade.save

    @mutex[:mutex].release!
    @mutex = EARMMS::MutexModel.acquire!(current_user.id, 'initial_setup')

    flash :success, t(:'initial_setup/discard_and_return/success')
    redirect '/initialsetup'
  end
end
