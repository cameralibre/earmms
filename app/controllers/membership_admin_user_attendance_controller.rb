class EARMMS::MembershipAdminUserAttendanceController < EARMMS::MembershipAdminController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:attendance'
  end
  
  get '/:id' do |uid|
    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile
    @user_name = @profile.decrypt(:full_name)

    page = request.params["page"].to_i
    page = 1 if page.nil? || page.zero?
    @ds = EARMMS::BranchMeetingAttendanceRecordFilter
      .perform_filter(:profile, @profile.id.to_s)
      .order(Sequel.desc(:id))
      .paginate(page, 25)

    @current_page = @ds.current_page
    @total_pages = @ds.page_count

    @attendance = @ds.map do |entry|
      at = EARMMS::BranchMeetingAttendanceRecord[entry.attendance_record]
      next nil unless at
      meeting = EARMMS::BranchMeeting[at.meeting]
      next nil unless meeting
      branch = EARMMS::Branch[meeting.branch]
      next nil unless branch

      status = at.decrypt(:attendance_status)
      status_friendly = t(:'meeting/attendance/unknown')
      if %w[present apologies].include?(status)
        status_friendly = t("meeting/attendance/#{status}".to_sym)
      end

      {
        :meetingid => meeting.id,
        :datetime => Time.parse(meeting.decrypt(:datetime)),
        :branch => {
          :obj => branch,
          :id => branch.id,
          :name => branch.decrypt(:name),
        },
        :status => {
          :type => status,
          :friendly => status_friendly,
        },
      }
    end.compact.sort{|a, b| b[:created] <=> a[:created]}

    @title = t(:'membership/user/attendance/title', :user => @user_name)
    haml :'membership_admin/user/attendance'
  end

  post '/:id/export' do |uid|
    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    EARMMS::Workers::AttendanceExport.queue(:type => "user", :user => @user.id, :requesting_user => current_user.id)
    flash :success, t(:'membership/user/attendance/export/success')

    redirect "/admin/user/attendance/#{uid}"
  end
end
