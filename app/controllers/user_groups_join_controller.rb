class EARMMS::UserGroupsJoinController < EARMMS::ApplicationController
  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      next redirect('/auth')
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)&.strip&.downcase

    # Get groups that the user is already a member of
    @my_groups = EARMMS::Group.hashes_for_user(@user)

    # Get all joinable groups, filtering out groups that:
    #   - the user is already a member of
    #   - are restricted to members only, if the user is not a member
    @joinable = EARMMS::Group.where(is_joinable: true).map do |g|
      next nil if @my_groups.map{|x| x[:group_id]}.include?(g.id)
      if g.restrict_to_member
        next nil unless %w[member].include?(@status)
      end
      
      g.to_h
    end.compact

    # And sort these by group type
    @joinable_typed = {}
    @joinable.each do |g|
      @joinable_typed[g[:type]] ||= []
      @joinable_typed[g[:type]] << g
    end
  end

  get '/' do
    @title = t(:'usergroups/joinable/title')
    haml :'user_groups/layout' do
      haml :'user_groups/join/index', :layout => false
    end
  end

  get '/:groupid' do |groupid|
    @group = EARMMS::Group[groupid.to_i]
    next halt 404 unless @group
    next halt 404 unless @joinable.map{|x| x[:group_id]}.include?(@group.id)
    @group_h = @group.to_h
    @title = t(:'usergroups/join/title', :type => t("group/type/#{@group_h[:type]}".to_sym), :name => @group_h[:name])

    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'user_groups/join/join', :layout => false
    end
  end

  post '/:groupid' do |groupid|
    @group = EARMMS::Group[groupid.to_i]
    next halt 404 unless @group
    next halt 404 unless @joinable.map{|x| x[:group_id]}.include?(@group.id)
    @group_h = @group.to_h

    # If there's an agreement for this group, bail unless it's been agreed to
    unless @group_h[:agreement].nil? || @group_h[:agreement].empty?
      unless request.params['agreement-confirm']&.strip&.downcase == 'on'
        flash :error, t(:'usergroups/join/errors/agreement_confirm')
        next redirect request.path
      end
    end

    # Quick "this was actually a form submission from what we rendered" check
    next redirect request.path unless request.params['validate'].to_i == 39

    # If we get here, we're okay to join the group!
    # Send an email to the group admins saying a new member has joined
    admins = @group.admins(include_notify_users: true)
    unless admins.empty?
      admin_emails = admins.map(&:email).compact

      text = EARMMS.email_templates.group_new_user({
        :group => @group_h,
        :user_name => @profile.decrypt(:full_name),
        :user_email => @user.email,
        :branch => EARMMS::Branch[@profile.decrypt(:branch).to_i]&.decrypt(:name),
      })

      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => admin_emails}))
      qm.encrypt(:subject, EARMMS.email_subject(t(:'group/userjoined/subject', :force_language => true)))
      qm.encrypt(:content, text.content_text)
      qm.encrypt(:content_html, text.content_html)
      qm.save
    end

    # Create GroupMember entry
    gm = EARMMS::GroupMember.new(group: @group.id)
    gm.save
    gm.encrypt(:user, @user.id)
    gm.encrypt(:roles, '')
    gm.save
    EARMMS::GroupMemberFilter.create_filters_for(gm)

    # Done!
    flash :success, t(:'usergroups/join/success', :name => @group_h[:name])
    next redirect "/ugroups/view/#{@group.id}"
  end
end
