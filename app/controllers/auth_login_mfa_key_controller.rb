require 'u2f'
require 'base64'

class EARMMS::AuthLoginMfaKeyController < EARMMS::ApplicationController
  before do
    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @title = t(:'auth/login/mfa/title')

    next halt redirect('/') if logged_in?

    next halt 400 unless session[:uid]
    @u = EARMMS::User[session[:uid].to_i]
    next halt 400 unless @u
    next halt 400 unless @u.totp_enabled

    @debug_bypass = settings.development? && (@u.decrypt(:totp_secret) == '#DEBUG BYPASS#')
    next halt redirect('/auth/mfa/totp') if @debug_bypass

    @has_u2f = EARMMS::U2FRegistration.where(user: @u.id).count.positive?
    next halt redirect('/auth/mfa/totp') unless @has_u2f

    @messages = {
      insert: t(:'mfa_key/status/insert'),
      error: t(:'auth/login/mfa/key/status/error'),
      success: t(:'mfa_key/status/success'),
    }

    if @enhanced = @u.enhanced_security
      @messages[:error] = t(:'auth/login/enhanced/key/status/error')
    end

    @u2f = U2F::U2F.new(request.base_url)
    @app_id = @u2f.app_id
    key_handles = EARMMS::U2FRegistration.where(user: @u.id).map(&:key_handle)
    @sign_requests = @u2f.authentication_requests(key_handles)
  end

  get '/' do
    session[:u2f_challenge] = @u2f.challenge
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/login/key', :layout => false
    end
  end

  post '/' do
    begin
      response = U2F::SignResponse.load_from_json(request.params['response']&.strip)
      reg = EARMMS::U2FRegistration.where(user: @u.id, key_handle: response.key_handle).first
      unless reg
        flash :error, t(:'auth/login/mfa/key/errors/invalid_key')
        next redirect request.path
      end
    rescue
      flash :error, t(:'auth/login/mfa/key/errors/unknown_error')
      next redirect request.path
    end

    begin
      @u2f.authenticate!(session[:u2f_challenge], response, Base64.decode64(reg.public_key), reg.counter)
    rescue U2F::Error => e
      flash :error, t(:'auth/login/mfa/key/errors/u2f_exception', ex: e.class.name)
      next redirect request.path
    ensure
      session.delete(:u2f_challenge)
    end

    # If we get here, login is successful
    flash :success, t(:'auth/login/success')

    # Update U2F key registration counter
    reg.counter = response.counter
    reg.save

    # And issue a token!
    token = EARMMS::Token.generate_session(@u)
    session.delete(:uid)
    session[:token] = token.token
    token.save

    redir_to = session.delete(:after_login)
    redir_to = '/' unless redir_to
    next redirect(redir_to)
  end
end
