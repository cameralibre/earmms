class EARMMS::AuthSignupExtendedBranchController < EARMMS::ApplicationController
  helpers EARMMS::AuthSignupHelpers
  helpers EARMMS::BranchHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect('/auth')
    end

    @current_step = "branch"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    extended_signup_verify!()
    @signup_data = extended_signup_get_data()
    @branches = get_branches_data(ignore_roles: true)
    
    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @title = t(:'auth/signup_extended/branch/title')
  end

  get '/' do
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/extended/branch', :layout => false
    end
  end
  
  post '/' do
    branch = EARMMS::Branch[request.params['branch'].to_i]
    unless branch
      flash :error, t(:'auth/signup_extended/branch/errors/invalid_branch')
      next redirect request.path
    end

    @signup_data["branch"] = branch.id
    @user.encrypt(:signup_extended_status_data, JSON.generate(@signup_data))
    @user.signup_extended_status = "profile"
    @user.save

    redirect extended_signup_current_step_url()
  end
end

