class EARMMS::MembershipAdminGroupController < EARMMS::MembershipAdminController
  helpers EARMMS::GroupHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group'

    @gtypes = get_groups_hash_by_type(:all, :link_to_edit => :admin, :admin => true)

    @title = t(:'membership/groups/title')
    haml :'membership_admin/group/index'
  end
end
