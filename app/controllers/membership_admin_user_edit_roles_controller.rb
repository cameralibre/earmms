class EARMMS::MembershipAdminUserEditRolesController < EARMMS::MembershipAdminController
  helpers EARMMS::BranchHelpers
  helpers EARMMS::MembershipAdminUserEditRolesHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:roles'

    @role_cats = get_available_roles()
  end
  
  get '/:uid' do |uid|
    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_roles = @user.get_roles.map{|x| x[:role]}
    @role_cats = filter_cats_by_user_roles(@role_cats, @user_roles)
    @has_2fa = @user.totp_enabled
    @user_name = @profile.decrypt(:full_name)

    @title = t(:'membership/user/roles/title', :user => @user_name)
    haml :'membership_admin/user/edit_roles'
  end

  post '/:uid' do |uid|
    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_roles = @user.get_roles.map{|x| x[:role]}
    @role_cats = filter_cats_by_user_roles(@role_cats, @user_roles)
    @has_2fa = @user.totp_enabled

    # Create a list of the roles to add and the roles to remove
    to_add = []
    to_remove = []
    @role_cats.each do |cat|
      cat[:roles].each do |role|
        roles = [role[:role]]
        roles = role[:role] if role[:multiple]

        r_enabled = request.params["role__#{role[:key]}"]&.strip&.downcase == "on"
        if r_enabled
          if @has_2fa
            roles.each do |r|
              to_add << r
            end
          end
        else
          roles.each do |r|
            to_remove << r
          end
        end
      end
    end

    # Action the removals
    to_remove.uniq.each do |r|
      if @user_roles.include?(r)
        EARMMS::UserHasRole.where(user: @user.id, role: r).delete
      end
    end
    
    # Update role list after removal
    @user_roles = @user.get_roles.map{|x| x[:role]}
    
    # Action the additions, checking for MFA and refusing to add new roles if
    # MFA is not enabled
    if @has_2fa
      to_add.uniq.each do |r|
        unless @user_roles.include?(r)
          EARMMS::UserHasRole.new(user: @user.id, role: r).save
        end
      end
    end

    flash :success, t(:'membership/user/roles/success')
    next redirect request.path
  end
end
