class EARMMS::MembershipAdminBranchEditController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminBranchEditHelpers

  get '/:branchid' do |branchid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'
    @branch = EARMMS::Branch[branchid.strip.to_i]
    next halt 404, "invalid branch" unless @branch

    @branch_name = @branch.decrypt(:name)
    if EARMMS.app_config['membership-default-branch']
      @branch_is_default = @branch.id == EARMMS.app_config['membership-default-branch'].to_i
    end

    @title = t(:'membership/branch/edit/title', :branch => @branch_name)
    haml :'membership_admin/branch/edit'
  end

  post '/:branchid' do |branchid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'
    @branch = EARMMS::Branch[branchid.strip.to_i]
    next halt 404, "invalid branch" unless @branch

    @branch_name = @branch.decrypt(:name)
    if EARMMS.app_config['membership-default-branch']
      @branch_is_default = @branch.id == EARMMS.app_config['membership-default-branch'].to_i
    end

    case request.params["action"]&.strip&.downcase
    when "name_change"
      name = request.params["name"]&.strip
      if name.nil? || name.empty?
        flash :error, t(:'membership/branch/edit/name_change/errors/no_name')
        next redirect request.path
      end

      oldname = @branch_name
      @branch_name = name
      @branch.encrypt(:name, name)
      @branch.save

      gen_alert :membership, :branch_edit, t(:'membership/branch/edit/name_change/success', {
        :bid => @branch.id,
        :new => @branch_name,
        :old => oldname,
        :force_language => true,
      })

      flash :success, t(:'membership/branch/edit/name_change/success', {
        :bid => @branch.id,
        :new => @branch_name,
        :old => oldname,
      })

    when "export"
      EARMMS::Workers::AttendanceExport.queue(:type => 'branch', :branch => @branch.id, :requesting_user => current_user.id)
      flash :success, t(:'membership/branch/edit/attendance_export/success')

    else
      flash :error, t(:'membership/branch/edit/errors/invalid_action')
    end

    redirect request.path
  end
end
