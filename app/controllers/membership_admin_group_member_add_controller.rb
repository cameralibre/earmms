class EARMMS::MembershipAdminGroupMemberAddController < EARMMS::MembershipAdminController
  helpers EARMMS::GroupHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group:modify_member'

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
  end

  get '/:id' do |id|
    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group
    @group_h = @group.to_h

    @title = t(:'membership/groups/members/add/title', :group => @group_h[:name])    
    haml :'membership_admin/group/member_add/index'
  end
  
  post '/:id' do |id|
    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group
    @group_h = @group.to_h
    @members = get_group_members(@group)

    @make_admin = request.params['make_admin']&.strip&.downcase == 'on'
    @user_adding = EARMMS::User[request.params['user_id'].to_i]
    if @user_adding.nil?
      flash :error, t(:'membership/groups/members/add/errors/invalid_uid')
      next redirect request.path
    end
    @user_adding_profile = EARMMS::Profile.for_user(@user_adding)
    @user_adding_name = @user_adding_profile.decrypt(:full_name)
    
    if @members.keys.include?(@user_adding.id)
      flash :error, t(:'membership/groups/members/add/errors/already_exists')
      next redirect request.path
    end
    
    # Check group membership status restriction
    if @group_h[:member_only] && @user_adding_profile.decrypt(:membership_status)&.strip&.downcase != 'member'
      flash :warning, t(:'membership/groups/members/add/errors/restricted_non_member')
    end

    if request.params['confirm']&.strip&.downcase == 'on'
      # Add user to group
      gm = EARMMS::GroupMember.new(group: @group.id)
      gm.save
      gm.encrypt(:user, @user_adding.id.to_s)
      if @make_admin
        gm.encrypt(:roles, 'admin,email') 
      end
      gm.save

      # Create filters
      EARMMS::GroupMemberFilter.create_filters_for(gm)

      # Success!
      flash :success, t(:'membership/groups/members/add/success', :user => @user_adding_name)
      next redirect "/admin/group/members/#{@group.id}"
    end

    @title = t(:'membership/groups/members/add/confirm/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'membership_admin/group/member_add/confirm', :layout => false
    end
  end
end
