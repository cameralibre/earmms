class EARMMS::IndexController < EARMMS::ApplicationController
  before do
    next redirect '/dashboard' if logged_in?

    @title = t(:'index/title')
  end

  get '/' do
    haml :'index/index'
  end
end
