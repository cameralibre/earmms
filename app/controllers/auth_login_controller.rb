class EARMMS::AuthLoginController < EARMMS::ApplicationController
  before do
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
  end

  get '/' do
    next redirect '/dashboard' if logged_in?

    @title = t(:'auth/login/title_element')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/login/index', :layout => false
    end
  end

  post '/' do
    next redirect '/dashboard' if logged_in?

    @email = request.params["email"]&.strip&.downcase
    @password = request.params["password"]
    if @email.nil? || @email.empty? || @password.nil? || @password.empty?
      flash :error, t(:'auth/login/errors/invalid')
      next redirect request.path
    end

    @user = EARMMS::User.where(email: @email).first
    unless @user&.verify_password(@password)
      flash :error, t(:'auth/login/errors/invalid')
      next redirect request.path
    end

    p = EARMMS::Profile.for_user(@user)
    l = p.decrypt(:preferred_language)
    session[:lang] = l unless (l.nil? || l.empty?)

    if @user.totp_enabled
      session[:uid] = @user.id

      # Redirect to U2F auth if the user has registered U2F keys, otherwise
      # redirect to TOTP
      if EARMMS::U2FRegistration.where(user: @user.id).count.positive?
        next redirect '/auth/mfa/key'
      else
        next redirect '/auth/mfa/totp'
      end

    else
      token = EARMMS::Token.generate_session(@user).save
      session[:token] = token.token

      flash(:success, t(:'auth/login/success'))

      redir_to = session.delete(:after_login)
      redir_to = '/' unless redir_to
      next redirect(redir_to)
    end
  end

  get '/logout' do
    redirect('/auth') unless logged_in?

    token = current_token()
    token.invalidate!
    token.save

    flash(:success, t(:'auth/logout/success'))
    redirect('/auth')
  end
end
