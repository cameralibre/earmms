class EARMMS::InitialSetupDebuggingController < EARMMS::InitialSetupController
  get '/' do
    @data = @this_upgrade.decrypt(:data)
    @data = '{}' if @data.nil? || @data.empty?
    @data = JSON.parse(@data)
    @data_display = JSON.pretty_generate(@data)
    @status_entries = [
      {
        :name => 'mode',
        :friendly => t(:'initial_setup/debugging/status/entry/mode'),
        :value => @this_upgrade.mode,
      },
      {
        :name => 'status',
        :friendly => t(:'initial_setup/debugging/status/entry/status'),
        :value => @this_upgrade.status,
      },
      {
        :name => 'has_v1_keys',
        :friendly => t(:'initial_setup/debugging/status/entry/has_v1_keys'),
        :value => @has_v1_keys,
      },
    ]

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/debugging', :layout => false
    end
  end
end
