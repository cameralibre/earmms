class EARMMS::MembershipAdminConfigurationQuickLinksController < EARMMS::MembershipAdminController
  helpers EARMMS::QuickLinkHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @enabled_entry = EARMMS::ConfigModel.where(key: 'dashboard-quick-links-enabled').first
    @attributes_entry = EARMMS::ConfigModel.where(key: 'dashboard-quick-links-attributes').first
    @content_entry = EARMMS::ConfigModel.where(key: 'dashboard-quick-links-content').first

    if @enabled_entry.nil? || @attributes_entry.nil? || @content_entry.nil?
      flash :error, t(:'membership/configuration/quick_links/errors/non_existent_entries')
      next redirect '/admin/config'
    end

    @enabled_cfg = (@enabled_entry.value == 'yes')
    @enabled_loaded = EARMMS.app_config[@enabled_entry.key]
    @attributes_parsed = JSON.parse(@attributes_entry.value)
    @content_parsed = JSON.parse(@content_entry.value)
    @preview = quick_links_data(override: {
      enabled: @enabled_cfg,
      attributes: @attributes_parsed,
      content: @content_parsed
    })

    @title = t(:'membership/configuration/quick_links/title')
    @mutex = EARMMS::MutexModel.acquire!(current_user.id, 'membership-dashboard-quick-links')

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-fullwidth]
  end

  helpers do
    def ql_mutex_guard!
      if @mutex[:mine]
        if @mutex[:new] == :new
          flash :success, t(:'membership/configuration/quick_links/lock/acquired')
        end
      else
        @mutex_user = {
          id: @mutex[:mutex].user,
          name: EARMMS::Profile.for_user(@mutex[:mutex].user)&.decrypt(:full_name),
        }

        @auth_header_hide_home = @auth_header_hide_links = true
        @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
        out = haml(:'auth/layout', :layout => :layout_minimal) do
          haml :'membership_admin/config/quick_links/locked', :layout => false
        end

        halt out
      end
    end
  end

  post '/release-lock' do
    if request.params['force'].to_i == 1
      @mutex[:mutex].release!
      next redirect '/admin/config/quick-links'
    end

    ql_mutex_guard!
    @mutex[:mutex].release!
    redirect '/admin/config'
  end

  get '/' do
    ql_mutex_guard!

    haml(:'auth/layout', :layout => :layout_minimal) do
      haml :'membership_admin/config/quick_links/index', :layout => false
    end
  end

  post '/toggle' do
    ql_mutex_guard!

    if request.params['enable'].to_i == 1
      if @enabled_entry.value == 'no'
        @enabled_entry.value = 'yes'
        @enabled_entry.save

        gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
          :key => @enabled_entry.key,
          :value => @enabled_entry.value,
          :force_language => true,
        }))
      end
    else
      if @enabled_entry.value == 'yes'
        @enabled_entry.value = 'no'
        @enabled_entry.save

        gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
          :key => @enabled_entry.key,
          :value => @enabled_entry.value,
          :force_language => true,
        }))
      end
    end

    flash :success, t(:'membership/configuration/quick_links/visibility/success', value: t("membership/configuration/quick_links/visibility/success/#{(@enabled_entry.value == 'yes') ? 'enabled' : 'disabled'}".to_sym))
    next redirect '/admin/config/quick-links'
  end

  post '/create' do
    ql_mutex_guard!

    text = request.params['text']&.strip
    link = request.params['link']&.strip
    icon = request.params['icon']&.strip
    icon = 'fa-link' if icon.nil? || icon.empty?
    colour = request.params['colour-type']&.strip&.downcase
    colour = request.params['colour']&.strip&.downcase if colour == 'custom'
    position = request.params['position']&.strip&.downcase
    only_members = request.params['only-members']&.strip&.downcase == 'on'

    req = [
      text.nil?(),
      text&.empty?(),
      link.nil?(),
      link&.empty?(),
      colour.nil?(),
      colour&.empty?(),
      position.nil?(),
      position&.empty?(),
    ].any?
    if req
      flash :error, t(:'membership/configuration/quick_links/create/errors/required_not_provided')
      next redirect '/admin/config/quick-links'
    end

    only_statuses = nil
    if only_members
      only_statuses = %w[member]
    end

    link_data = {
      "text" => text,
      "href" => link,
      "colour" => colour,
      "icon" => icon,
      "only_statuses" => only_statuses,
    }

    parsed = @content_parsed.dup
    parsed.insert(position.to_i, link_data)
    @content_entry.value = JSON.generate(parsed)
    @content_entry.save

    gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
      :key => @content_entry.key,
      :value => @content_entry.value,
      :force_language => true,
    }))

    flash :success, t(:'membership/configuration/quick_links/create/success')
    redirect '/admin/config/quick-links'
  end

  get '/edit/:pos' do |pos|
    ql_mutex_guard!

    @position = pos.to_i
    @selected = @content_parsed[@position]
    unless @selected
      flash :error, t(:'membership/configuration/quick_links/edit/errors/invalid_pos')
      next redirect '/admin/config/quick-links'
    end
    @only_members = false
    @only_members = @selected['only_statuses'].include?('member') if @selected['only_statuses'].respond_to?(:include?)

    haml(:'auth/layout', :layout => :layout_minimal) do
      haml :'membership_admin/config/quick_links/edit', :layout => false
    end
  end

  post '/edit/:pos' do |pos|
    ql_mutex_guard!

    @position = pos.to_i
    @selected = @content_parsed[@position]
    unless @selected
      flash :error, t(:'membership/configuration/quick_links/edit/errors/invalid_pos')
      next redirect '/admin/config/quick-links'
    end

    text = request.params['text']&.strip
    link = request.params['link']&.strip
    icon = request.params['icon']&.strip
    icon = 'fa-link' if icon.nil? || icon.empty?
    colour = request.params['colour-type']&.strip&.downcase
    colour = request.params['colour']&.strip&.downcase if colour == 'custom'
    position = request.params['position']&.strip&.downcase
    only_members = request.params['only-members']&.strip&.downcase == 'on'

    req = [
      text.nil?(),
      text&.empty?(),
      link.nil?(),
      link&.empty?(),
      colour.nil?(),
      colour&.empty?(),
      position.nil?(),
      position&.empty?(),
    ].any?
    if req
      flash :error, t(:'membership/configuration/quick_links/edit/errors/required_not_provided')
      next redirect '/admin/config/quick-links'
    end

    @selected['text'] = text
    @selected['href'] = link
    @selected['colour'] = colour
    @selected['icon'] = icon
    @selected.delete('only_statuses')
    if only_members
      @selected['only_statuses'] = %w[member]
    end

    @newposition = position.to_i
    @newposition = 0 if @newposition < 0
    @newposition = (@content_parsed.length - 1) if @newposition > @content_parsed.length

    parsed = @content_parsed.dup
    parsed.delete_at(@position)
    parsed.insert(@newposition, @selected)
    @content_entry.value = JSON.generate(parsed)
    @content_entry.save

    gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
      :key => @content_entry.key,
      :value => @content_entry.value,
      :force_language => true,
    }))

    flash :success, t(:'membership/configuration/quick_links/edit/success')
    redirect "/admin/config/quick-links/edit/#{@newposition}"
  end

  post '/delete/:pos' do |pos|
    ql_mutex_guard!

    @position = pos.to_i
    @selected = @content_parsed[@position]
    unless @selected
      flash :error, t(:'membership/configuration/quick_links/delete/errors/invalid_pos')
      next redirect '/admin/config/quick-links'
    end

    parsed = @content_parsed.dup
    parsed.delete_at(@position)
    @content_entry.value = JSON.generate(parsed)
    @content_entry.save

    gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
      :key => @content_entry.key,
      :value => @content_entry.value,
      :force_language => true,
    }))

    flash :success, t(:'membership/configuration/quick_links/delete/success')
    redirect '/admin/config/quick-links'
  end
end
