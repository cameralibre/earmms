require 'base64'
require 'json'
require 'addressable'

class EARMMS::InitialSetupUpgradeParseChangesController < EARMMS::InitialSetupController
  get '/' do
    begin
      @blob = JSON.parse(@this_upgrade.decrypt(:data))
    rescue
      # This should never happen!
      next halt 500
    end

    # Gather changes, and create a "mock database" hash to run the parse on
    @changes = upgrade_construct_changes(@blob)
    @mockdb = upgrade_construct_mock_db(@changes)

    # Run a parse run on the mock configuration
    @output = @mockdb.map do |key, desc|
      next if desc.nil?

      value = desc[:value]
      if desc[:type] == :bool
        value = (value ? 'yes' : 'no')
      end
      value = value.to_s

      parsed = value
      warnings = []
      stop = false
      EARMMS::Config.parsers.each do |parser|
        if !stop && parser.accept?(key, desc[:type])
          out = parser.parse(value)
          warnings << out[:warning] if out[:warning]
          parsed = out[:data]
          stop = out[:stop_processing_here]
        end
      end

      {
        :key => key,
        :warnings => warnings,
      }
    end.compact

    # Do we have warnings?
    @has_warnings = @output.map{|x| x[:warnings].count.positive?}.any?
    @warning_sources = {}
    @warning_sources[:blob] = @output.map do |x|
      next nil unless @mockdb[x[:key]][:source] == :blob
      x[:warnings].count.positive?
    end.compact.any?
    @warning_sources[:upgrade] = @output.map do |x|
      next nil unless @mockdb[x[:key]][:source] == :upgrade
      x[:warnings].count.positive?
    end.compact.any?
    @warning_sources[:default] = @output.map do |x|
      next nil unless @mockdb[x[:key]][:source] == :default
      x[:warnings].count.positive?
    end.compact.any?
    @warning_sources[:existing] = @output.map do |x|
      next nil unless @mockdb[x[:key]][:source] == :existing
      x[:warnings].count.positive?
    end.compact.any?

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/upgrade/layout', :layout => false do
        haml :'initial_setup/upgrade/parse', :layout => false
      end
    end
  end

  post '/' do
    # Parse output was accepted, save status as 'parsed' and redirect to the
    # "post-parse" test
    @this_upgrade.status = 'parsed'
    @this_upgrade.save
    next redirect current_step_url
  end
end
