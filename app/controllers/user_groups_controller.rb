class EARMMS::UserGroupsController < EARMMS::ApplicationController
  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      next redirect('/auth')
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    # Get groups
    @my_groups = EARMMS::Group.hashes_for_user(@user)
    @my_groups_typed = {}
    @my_groups.each do |g|
      @my_groups_typed[g[:type]] ||= []
      @my_groups_typed[g[:type]] << g
    end
  end

  get '/' do
    @title = t(:'usergroups/title')
    haml :'user_groups/layout' do
      haml :'user_groups/index', :layout => false
    end
  end

  get '/view/:groupid' do |groupid|
    @group = EARMMS::Group[groupid.to_i]
    next halt 404 unless @group
    @group_h = @my_groups.select{|x| x[:group_id] == @group.id}.first
    next halt 404 unless @group_h

    @title = t(:'usergroups/view/title', :type => t("group/type/#{@group_h[:type]}".to_sym), :name => @group_h[:name])
    haml :'user_groups/layout' do
      haml :'user_groups/view', :layout => false
    end
  end
end
