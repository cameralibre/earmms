class EARMMS::MembershipAdminConfigurationWelcomeEmailController < EARMMS::MembershipAdminController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @enabled_entry = EARMMS::ConfigModel.where(key: 'membership-welcome-email-enabled').first
    @subject_entry = EARMMS::ConfigModel.where(key: 'membership-welcome-email-subject').first
    @body_entry = EARMMS::ConfigModel.where(key: 'membership-welcome-email-body').first

    if @enabled_entry.nil? || @subject_entry.nil? || @body_entry.nil?
      flash :error, t(:'membership/configuration/welcome_email/errors/non_existent_entries')
      next redirect '/admin/config'
    end

    @title = t(:'membership/configuration/welcome_email/title')
  end

  get '/' do
    haml :'membership_admin/config/welcome_email'
  end

  post '/' do
    enabled = request.params['enabled']&.strip&.downcase == 'yes'
    if enabled
      if @enabled_entry.value == 'no'
        @enabled_entry.value = 'yes'
        @enabled_entry.save
        
        gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
          :key => @enabled_entry.key,
          :value => @enabled_entry.value,
          :force_language => true,
        }))
      end
    else
      if @enabled_entry.value == 'yes'
        @enabled_entry.value = 'no'
        @enabled_entry.save
        
        gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
          :key => @enabled_entry.key,
          :value => @enabled_entry.value,
          :force_language => true,
        }))
      end
        
      flash :success, t(:'membership/configuration/welcome_email/success/disabled')
      next redirect request.path
    end

    # change subject
    subject_changed = false
    subject = request.params["subject"]&.strip
    subject_fragment = Sanitize.fragment(subject).gsub("&nbsp;", " ").strip
    if subject.nil? || subject&.empty? || subject_fragment.empty?
      subject_changed = true if @subject_entry.value != ''
      @subject_entry.value = ''
    else
      subject_changed = true if @subject_entry.value != subject
      @subject_entry.value = subject
    end

    if subject_changed
      @subject_entry.save
      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @subject_entry.key,
        :value => @subject_entry.value,
        :force_language => true,
      }))
    end

    # change body
    body_changed = false
    body = request.params["body"]&.strip
    body_fragment = Sanitize.fragment(body, Sanitize::Config::RESTRICTED).gsub("&nbsp;", " ").strip
    if body.nil? || body&.empty? || body_fragment.empty?
      body_changed = true if @body_entry.value != ''
      @body_entry.value = ''
    else
      body_changed = true if @body_entry.value != body
      @body_entry.value = body
    end

    if body_changed
      @body_entry.save
      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @body_entry.key,
        :value => @body_entry.value,
        :force_language => true,
      }))
    end

    flash :success, t(:'membership/configuration/welcome_email/success')
    next redirect request.path
  end
end
