class EARMMS::SystemApiTokenController < EARMMS::SystemController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:apitokens'

    @user = current_user
    @my_apitokens = EARMMS::Token.where(use: 'apitoken', user: @user.id, valid: true).map do |t|
      next nil unless t.is_valid?

      data = begin
        JSON.parse(t.extra_data)
      rescue
        t.invalidate!
        next nil
      end

      {
        id: t.id,
        name: data["name"] || t(:'unknown'),
        created: t.created,
        expiry: t.expiry,
      }
    end.compact
  end

  get '/' do
    @title = t(:'system/apitokens/title')
    haml :'system/apitokens/index'
  end

  get '/all' do
    @all_apitokens = {}
    EARMMS::Token.where(use: 'apitoken', valid: true).each do |t|
      next nil unless t.is_valid?

      data = begin
        JSON.parse(t.extra_data)
      rescue
        t.invalidate!
        next nil
      end

      @all_apitokens[t.user.to_s] ||= []
      @all_apitokens[t.user.to_s] << {
        id: t.id,
        name: data["name"] || t(:'unknown'),
        created: t.created,
        expiry: t.expiry,
      }
    end

    @user_map = @all_apitokens.keys.map do |uid|
      user = EARMMS::User[uid.to_i]
      profile = EARMMS::Profile.for_user(user)
      name = profile.decrypt(:full_name)
      if name.nil? || name&.empty?
        name = t(:'unknown')
      end

      [uid, name]
    end.to_h

    @title = t(:'system/apitokens/all/title')
    haml :'system/apitokens/all'
  end

  get '/generate' do
    @title = t(:'system/apitokens/generate/title')
    haml :'system/apitokens/generate'
  end

  post '/generate' do
    @token_name = request.params['name']&.strip
    unless @token_name
      flash :error, t(:'system/apitokens/generate/errors/no_name')
      next redirect request.path
    end

    @token = EARMMS::Token.generate_full
    @token.user = @user.id
    @token.use = 'apitoken'
    @token.extra_data = JSON.generate({"name" => @token_name})
    @token.save

    flash :success, t(:'system/apitokens/generate/success', :name => @token_name, :token => @token.token)
    redirect '/system/apitokens'
  end

  post '/revoke' do
    token = EARMMS::Token[request.params['token']&.strip.to_i]
    next halt 404 unless token
    next halt 404 unless token.use == 'apitoken'

    data = begin
      JSON.parse(token.extra_data)
    rescue
      {"name" => "(unknown)"}
    end

    token.invalidate!

    flash :success, t(:'system/apitokens/revoke/success', :name => data["name"])
    next redirect '/system/apitokens'
  end
end
