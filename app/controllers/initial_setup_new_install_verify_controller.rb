class EARMMS::InitialSetupNewInstallVerifyController < EARMMS::InitialSetupController
  get '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @configs = newinstall_cfg_entries(@data['config-entries']).map{|k,v| v}.flatten
    @roles = @data['roles'].map do |uid, d|
      user = EARMMS::User[uid.to_i]
      next unless user

      {
        :uid => user.id,
        :email => user.email,
        :add => d['add'],
        :remove => d['remove'],
      }
    end

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/verify', :layout => false
      end
    end
  end

  post '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.status = 'verify-ok'
    @this_upgrade.save
    next redirect current_step_url
  end
end
