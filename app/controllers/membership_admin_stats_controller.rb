class EARMMS::MembershipAdminStatsController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminStatsHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:access'

    @branches = EARMMS::Branch.all.map{|b| [b.id.to_s, b.decrypt(:name)]}.to_h
    @since = Chronic.parse(request.params["since"]&.strip&.downcase)
    @since ||= DateTime.now - 7
    @stats = stats_from_date(@since)
  end

  get '/' do
    @chart_by_day = @stats[:by_day].map do |day|
      {
        date: day[:day].strftime("%Y-%m-%d"),
        members: day[:overall][:members],
        supporters: day[:overall][:supporters],
      }
    end

    @chart_json = {
      type: 'line',
      data: {
        labels: @chart_by_day.map{|x| x[:date]},
        datasets: [
          {
            label: t(:'membership_status/member/plural'),
            data: @chart_by_day.map{|x| x[:members]},
            borderColor: t(:'membership/stats/graph_colour/members'),
            lineTension: 0.1,
          },
          {
            label: t(:'membership_status/supporter/plural'),
            data: @chart_by_day.map{|x| x[:supporters]},
            borderColor: t(:'membership/stats/graph_colour/supporters'),
            lineTension: 0.1,
          },
        ]
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        },
      },
    }

    @title = t(:'membership/stats/title')
    haml :'membership_admin/stats/index'
  end

  get '/detail' do
    @show = {
      overall: true,
      branches: @branches.keys.map do |x|
        [x, true]
      end.to_h,
    }

    @title = t(:'membership/stats/detail/title')
    haml :'membership_admin/stats/detail'
  end

  post '/detail' do
    @show = {
      overall: request.params['show_overall']&.strip&.downcase == 'on',
      branches: @branches.keys.map do |x|
        [
          x,
          request.params["show_br_#{x}"]&.strip&.downcase == 'on',
        ]
      end.to_h,
    }

    @title = t(:'membership/stats/detail/title')
    haml :'membership_admin/stats/detail'
  end
end
