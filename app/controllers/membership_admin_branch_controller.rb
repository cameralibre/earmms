class EARMMS::MembershipAdminBranchController < EARMMS::MembershipAdminController
  helpers EARMMS::BranchHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'
    @branches = get_branches_data(ignore_roles: true)
  end
  
  get '/' do
    @title = t(:'membership/branch/title')
    haml :'membership_admin/branch/index'
  end
end
