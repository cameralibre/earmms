class EARMMS::BranchMeeting < Sequel::Model(:branch_meetings)
  def send_reminder!
    branch = EARMMS::Branch[self.branch]
    return nil unless branch

    datetime = DateTime.parse(self.decrypt(:datetime))
    prettydate = datetime.strftime("%Y-%m-%d")

    text = EARMMS.email_templates.branch_meeting(branch, self)

    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "mass_email_target", "target" => "branch:#{branch.id}:member"}))
    qm.encrypt(:subject, EARMMS.email_subject("#{branch.decrypt(:name)} meeting on #{prettydate}"))
    qm.encrypt(:content, text.content_text)
    qm.encrypt(:content_html, text.content_html)
    qm.save
  end
end
