require 'chronic'
require 'base32'

class EARMMS::Token < Sequel::Model
  FULL_TOKEN_LENGTH = (EARMMS::Crypto::TOKEN_LENGTH * 2)
  SHORT_TOKEN_LENGTH = 16

  def self.generate_full
    t = EARMMS::Crypto.generate_token
    self.new(token: t, created: Time.now, expiry: nil, valid: true)
  end

  def self.generate_short
    t = EARMMS::Crypto.hex_to_bin(EARMMS::Crypto.generate_token())
    t = Base32.encode(t).downcase
    t = t[0..(SHORT_TOKEN_LENGTH - 1)]

    self.new(token: t, created: Time.now, expiry: nil, valid: true)
  end

  def self.generate_session(user)
    user = user.id if user.respond_to?(:id)

    token = self.generate_full
    token.user = user
    token.use = 'session'
    token.save
  end

  def self.generate_password_reset(user)
    user = user.id if user.respond_to?(:id)

    token = self.generate_full
    token.user = user
    token.use = 'password_reset'
    token.save
  end

  def self.generate_email_verify(user)
    user = user.id if user.respond_to?(:id)

    token = self.generate_full
    token.user = user
    token.use = 'email_verify'
    token.expiry = Chronic.parse('in 12 hours')
    token.save
  end

  def self.generate_delete_confirm(user)
    user = user.id if user.respond_to?(:id)

    token = self.generate_full
    token.user = user
    token.use = 'delete_confirm'
    token.expiry = Chronic.parse('in 24 hours')
    token.save
  end

  def self.generate_invite(data = {})
    token = self.generate_short
    token.use = 'invite'
    token.extra_data = JSON.generate(data)
    token.save
  end

  def is_valid?
    orig = self.valid
    if self.expiry && Time.now >= self.expiry
      self.valid = false
    end

    self.save unless orig == self.valid
    self.valid
  end

  def invalidate!
    self.expiry = Time.now
    self.valid = false
    self.save
  end
end
